part of ui.helper;

class UIPadding {
  UIPadding._();

  static const EdgeInsetsGeometry paddingH4 =
      EdgeInsets.symmetric(horizontal: UILayout.xsmall);
  static const EdgeInsetsGeometry paddingV4 =
      EdgeInsets.symmetric(vertical: UILayout.xsmall);
  static const EdgeInsetsGeometry padding_4 = EdgeInsets.all(UILayout.xsmall);

  static const EdgeInsetsGeometry paddingH8 =
      EdgeInsets.symmetric(horizontal: UILayout.small);
  static const EdgeInsetsGeometry paddingV8 =
      EdgeInsets.symmetric(vertical: UILayout.small);
  static const EdgeInsetsGeometry padding_8 = EdgeInsets.all(UILayout.small);

  static const EdgeInsetsGeometry paddingH16 =
      EdgeInsets.symmetric(horizontal: UILayout.medium);
  static const EdgeInsetsGeometry paddingV16 =
      EdgeInsets.symmetric(vertical: UILayout.medium);
  static const EdgeInsetsGeometry padding_16 = EdgeInsets.all(UILayout.medium);

  static const EdgeInsetsGeometry paddingH24 =
      EdgeInsets.symmetric(horizontal: UILayout.large);
  static const EdgeInsetsGeometry paddingV24 =
      EdgeInsets.symmetric(vertical: UILayout.large);
  static const EdgeInsetsGeometry padding_24 = EdgeInsets.all(UILayout.large);

  static const EdgeInsetsGeometry paddingH32 =
      EdgeInsets.symmetric(horizontal: UILayout.mlarge);
  static const EdgeInsetsGeometry paddingV32 =
      EdgeInsets.symmetric(vertical: UILayout.mlarge);
  static const EdgeInsetsGeometry padding_32 = EdgeInsets.all(UILayout.mlarge);

  static const EdgeInsetsGeometry paddingH48 =
      EdgeInsets.symmetric(horizontal: UILayout.xlarge);
  static const EdgeInsetsGeometry paddingV48 =
      EdgeInsets.symmetric(vertical: UILayout.xlarge);
  static const EdgeInsetsGeometry padding_48 = EdgeInsets.all(UILayout.xlarge);

  static const EdgeInsetsGeometry paddingH96 =
      EdgeInsets.symmetric(horizontal: UILayout.xxlarge);
  static const EdgeInsetsGeometry paddingV96 =
      EdgeInsets.symmetric(vertical: UILayout.xxlarge);
  static const EdgeInsetsGeometry padding_96 = EdgeInsets.all(UILayout.xxlarge);

  /// [ui-padding]
}
