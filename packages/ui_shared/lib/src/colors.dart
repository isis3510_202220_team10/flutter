part of ui.helper;

class UIColors {
  /// [Primary-Group]
  static const Color eggshell = Color(0xfff4ebd1);
  static const Color middleRed = Color(0xffe38d72);
  static const Color blackCoffee = Color(0xff362c28);
  static const Color mediumSeaGreen = Color(0xff4cae63);
  static const Color error = Color(0xffff574d);
  static const Color gray = Color.fromARGB(255, 162, 140, 132);
}
