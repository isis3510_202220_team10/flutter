part of ui.helper;

/// [Headers-group]
TextStyle headers = const TextStyle(
  fontFamily: 'Averia Serif Libre',
  letterSpacing: 0,
);
TextStyle h0 = headers.copyWith(fontSize: 48, fontWeight: FontWeight.w400);
TextStyle h1 = headers.copyWith(fontSize: 40, fontWeight: FontWeight.w400);
TextStyle h2 = headers.copyWith(fontSize: 32, fontWeight: FontWeight.w400);
TextStyle h3 = headers.copyWith(fontSize: 28, fontWeight: FontWeight.w400);
TextStyle h4 = headers.copyWith(fontSize: 24, fontWeight: FontWeight.w400);
TextStyle h5 = headers.copyWith(fontSize: 20, fontWeight: FontWeight.w400);
TextStyle h0b = headers.copyWith(fontSize: 48, fontWeight: FontWeight.bold);
TextStyle h1b = headers.copyWith(fontSize: 40, fontWeight: FontWeight.bold);
TextStyle h2b = headers.copyWith(fontSize: 32, fontWeight: FontWeight.bold);
TextStyle h3b = headers.copyWith(fontSize: 28, fontWeight: FontWeight.bold);
TextStyle h4b = headers.copyWith(fontSize: 24, fontWeight: FontWeight.bold);
TextStyle h5b = headers.copyWith(fontSize: 20, fontWeight: FontWeight.bold);

/// [Short-Statements-Group]
TextStyle shortStatements = const TextStyle(
  fontFamily: 'Averia Serif Libre',
  letterSpacing: 0,
  fontWeight: FontWeight.bold,
);
TextStyle bodyLargeBold = shortStatements.copyWith(fontSize: 32);
TextStyle bodyMediumBold = shortStatements.copyWith(fontSize: 20);
TextStyle bodySmallBold = shortStatements.copyWith(fontSize: 16);
TextStyle bodyTinyBold = shortStatements.copyWith(fontSize: 14);
TextStyle bodyXSmallBold = shortStatements.copyWith(fontSize: 12);

/// [Paragraph-Group]
TextStyle paragraph = const TextStyle(
  fontFamily: 'Averia Serif Libre',
  letterSpacing: 0,
);

TextStyle bodyLargeRegular = paragraph.copyWith(fontSize: 20);
TextStyle bodyMediumRegular = paragraph.copyWith(fontSize: 16);
TextStyle bodySmallRegular = paragraph.copyWith(fontSize: 14);
TextStyle bodyXSmallRegular = shortStatements.copyWith(fontSize: 12);

/// [Italic Group]
TextStyle italicStatements = const TextStyle(
  fontFamily: 'Averia Serif Libre',
  letterSpacing: 0,
  fontStyle: FontStyle.italic,
);

TextStyle bodyLargeItalic = italicStatements.copyWith(fontSize: 32);
TextStyle bodyMediumItalic = italicStatements.copyWith(fontSize: 20);
TextStyle bodySmallItalic = italicStatements.copyWith(fontSize: 16);
TextStyle bodyTinyItalic = italicStatements.copyWith(fontSize: 14);

/// [Italic Group]
TextStyle italicBoldStatements = const TextStyle(
  fontFamily: 'Averia Serif Libre',
  letterSpacing: 0,
  fontStyle: FontStyle.italic,
);

TextStyle bodyLargeItalicBold = italicBoldStatements.copyWith(fontSize: 32);
TextStyle bodyMediumItalicBold = italicBoldStatements.copyWith(fontSize: 20);
TextStyle bodySmallItalicBold = italicBoldStatements.copyWith(fontSize: 16);
TextStyle bodyTinyItalicBold = italicBoldStatements.copyWith(fontSize: 14);