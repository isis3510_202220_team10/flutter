part of ui.helper;

EdgeInsets devicePadding(BuildContext appContext) =>
    MediaQuery.of(appContext).padding;

/// [+box-decoration]
final BoxDecoration cardWhite = BoxDecoration(
  color: Colors.white,
  boxShadow: const <BoxShadow>[shadowB2],
  borderRadius: radius4,
);

BoxDecoration cardWhiteWithBorder({
  required Color borderColor,
  double width = 1,
}) =>
    BoxDecoration(
      color: Colors.white,
      boxShadow: const <BoxShadow>[shadowB2],
      border: Border.all(width: width, color: borderColor),
      borderRadius: radius4,
    );

BoxDecoration cardWhiteWitoutBoder({
  required Color borderColor,
  double width = 1,
}) =>
    BoxDecoration(
      color: Colors.white,
      boxShadow: const <BoxShadow>[shadowB2],
      border: Border.all(width: width, color: borderColor),
      borderRadius: BorderRadius.circular(0),
    );

class ClipShadowPath extends StatelessWidget {
  const ClipShadowPath({
    required this.shadow,
    required this.clipper,
    required this.child,
    Key? key,
  }) : super(key: key);
  final Shadow shadow;
  final CustomClipper<Path> clipper;
  final Widget child;

  @override
  Widget build(BuildContext context) => CustomPaint(
        painter: _ClipShadowShadowPainter(
          clipper: clipper,
          shadow: shadow,
        ),
        child: ClipPath(
          clipper: clipper,
          child: child,
        ),
      );
}

class _ClipShadowShadowPainter extends CustomPainter {
  _ClipShadowShadowPainter({
    required this.shadow,
    required this.clipper,
  });
  final Shadow shadow;
  final CustomClipper<Path> clipper;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = shadow.toPaint();
    Path clipPath = clipper.getClip(size).shift(shadow.offset);
    canvas.drawPath(clipPath, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
