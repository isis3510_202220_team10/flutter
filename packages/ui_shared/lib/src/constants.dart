part of ui.helper;

/// [+boders]
const BorderSide border1 = BorderSide(width: 1);
const BorderSide border2 = BorderSide(width: 2);

/// [+border-radius]
final BorderRadius radius4 = BorderRadius.circular(4);
const BorderRadius radiust4 = BorderRadius.only(
  topLeft: Radius.circular(4),
  topRight: Radius.circular(4),
);
const BorderRadius radiust16 = BorderRadius.only(
  topLeft: Radius.circular(16),
  topRight: Radius.circular(16),
);
const BorderRadius radiusl4 = BorderRadius.only(
  topLeft: Radius.circular(4),
  bottomLeft: Radius.circular(4),
);
const BorderRadius radiusr4 = BorderRadius.only(
  topRight: Radius.circular(4),
  bottomRight: Radius.circular(4),
);
final BorderRadius radius8 = BorderRadius.circular(8);
final BorderRadius radius16 = BorderRadius.circular(16);

/// [+box-shadow]
const BoxShadow shadowB4 = BoxShadow(
  color: Color(0x0f000000), // color-name
  offset: Offset(0, 2),
  blurRadius: 4,
  spreadRadius: 0,
);

const BoxShadow shadowB2 = BoxShadow(
  color: Color(0x0f000000), // color-name
  offset: Offset(0, 1),
  blurRadius: 2,
  spreadRadius: 0,
);

BoxShadow customShadow({
  required double x,
  required double y,
  required double b,
  required double s,
}) =>
    BoxShadow(
      color: const Color(0x0f000000), // color-name
      offset: Offset(x, y),
      blurRadius: b,
      spreadRadius: s,
    );

Widget get loading => const Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      ),
    );
