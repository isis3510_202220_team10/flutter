part of ui.helper;

class UITiming {
  static UITiming instance = UITiming();
  Duration standard = const Duration(milliseconds: 300);
  Duration formStepper = const Duration(milliseconds: 600);
}
