part of ui.helper;

class UILayout {
  UILayout._();

  static const double xsmall = 4;
  static const double small = 8;
  static const double medium = 16;
  static const double large = 24;
  static const double mlarge = 32;
  static const double xlarge = 48;
  static const double xmlarge = 64;
  static const double xxlarge = 96;

  static const double imageSize = 124;
  static const double iconSizeSmall = 14;
  static const double iconSize = 32;

  static const double aspectRatio_16_9 = 16 / 9;
  static const double aspectRatio_3_2 = 3 / 2;
  static const double aspectRatio_4_3 = 4 / 3;
  static const double aspectRatio_1_1 = 1 / 1;
  static const double aspectRatio_3_3 = 3 / 4;
  static const double aspectRatio_2_3 = 2 / 3;

  static dynamic deviceAspectRatio(BuildContext appContext) =>
      MediaQuery.of(appContext).devicePixelRatio;

  static bool isIpadScreen(BuildContext context) =>
      MediaQuery.of(context).size.width < 768;

  static bool isSmallScreen(BuildContext context) =>
      MediaQuery.of(context).size.width < 960;

  static bool isMediumScreen(BuildContext context) =>
      MediaQuery.of(context).size.width > 960 &&
      MediaQuery.of(context).size.width < 1200;

  static bool isLargeScreen(BuildContext context) =>
      MediaQuery.of(context).size.width >= 1200;
}
