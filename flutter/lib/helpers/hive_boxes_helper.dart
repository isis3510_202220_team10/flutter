import 'package:hive/hive.dart';
import 'package:main_app/models/ingredient_model.dart';
import 'package:main_app/models/recipe_model.dart';
import 'package:main_app/models/user_model.dart';
import 'package:main_app/services/user_service.dart';

class HiveBoxesHelper {
  static final UserService userService = UserService();

  static Box<RecipeModel> getRecipes() =>
      Hive.box<RecipeModel>(RecipeModel.hiveBoxName);

  static Box<IngredientModel> getShoppingList() =>
      Hive.box<IngredientModel>(IngredientModel.shoppingListHiveBoxName);

  static Box<UserModel> getUsers() =>
      Hive.box<UserModel>(UserModel.hiveBoxName);

  static void addIngredientsToShoppingList(List<IngredientModel> ingredients) {
    Box<IngredientModel> shoppingList = getShoppingList();
    IngredientModel act;
    IngredientModel? stored;
    for (int i = 0; i < ingredients.length; i++) {
      act = ingredients[i];
      stored = shoppingList.get(act.reference);
      if (stored != null && stored.quantity != null && act.quantity != null) {
        act.quantity = act.quantity! + stored.quantity!;
      }
      shoppingList.put(act.reference, act);
    }
  }

  static UserModel? getUser(String id) => getUsers().get(id);
  static void putUser(UserModel user) => getUsers().put(user.id, user);

  static void editShoppingListIngredient(IngredientModel ingredient) =>
      getShoppingList().put(ingredient.reference, ingredient);

  static void deleteIngredientFromShoppingList(String reference) =>
      getShoppingList().delete(reference);
}
