class ErrorHelper {
  static String getErrorMessage(Object? error) {
    String resp = 'Ha ocurrido un error';
    String errorMsg = error.toString();
    if (errorMsg.toLowerCase().contains('invalid image data') ||
        errorMsg.toLowerCase().contains('no host specified')) {
      resp = 'La imagén ya no existe';
    } else if (errorMsg.toLowerCase().contains(
        'no has consumido nada por lo que no hay información para mostrar')) {
      resp =
          'Aún no has consumido nada por lo que no hay información para mostrar';
    }
    return resp;
  }
}
