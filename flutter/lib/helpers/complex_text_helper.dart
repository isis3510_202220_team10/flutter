import 'package:main_app/models/ingredient_model.dart';

class ComplexTextHelper {
  static String getIngredientsUnits(IngredientModel ingredient) =>
      '${getSimplifiedIngredientsUnits(ingredient)} de ${ingredient.name}';
  static String getSimplifiedIngredientsUnits(IngredientModel ingredient) =>
      '${ingredient.quantity ?? ''} ${ingredient.unit != null ? ingredient.unit!.replaceAll('de', '') : ''}';
  static String getUnitsShoppingList(IngredientModel ingredient) {
    String resp = '';
    if (ingredient.unit == null ||
        ingredient.unit!.toLowerCase().contains('gusto')) {
      resp = 'Al gusto';
    } else if (ingredient.unit!.toLowerCase().contains('unidad') &&
        ingredient.quantity != null) {
      resp = ingredient.quantity == 1 ? 'Unidad' : 'Unidades';
    } else {
      resp = ingredient.unit!;
    }
    return resp;
  }
}
