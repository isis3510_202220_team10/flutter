part of ui.views.screens;

class ShoppingCartScreen extends StatelessWidget {
  const ShoppingCartScreen({Key? key}) : super(key: key);
  static const routeName = '/shopping_cart';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            size: UILayout.large,
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        title: Text(
          'Carrito de compras',
          style: h2b.copyWith(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: UILayout.mlarge,
          vertical: UILayout.medium,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Dirección de entrega: ',
                  style: bodySmallItalic.copyWith(
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                Text(
                  'Calle 21 # 3 - 71',
                  style: bodySmallItalic.copyWith(
                    color: UIColors.mediumSeaGreen,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4)),
                    primary: Theme.of(context).primaryColor,
                  ),
                  child: Padding(
                    padding: UIPadding.paddingH4,
                    child: Text(
                      'Cambiar',
                      style: bodySmallItalic.copyWith(
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            UISpacing.spacingV16,
            Text(
              'Tiempo de entrega: ',
              style: bodySmallItalic.copyWith(
                color: Theme.of(context).primaryColor,
              ),
            ),
            UISpacing.spacingV4,
            Row(
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Theme.of(context).colorScheme.onPrimary,
                    ),
                  ),
                  child: const SizedBox(
                    height: UILayout.medium,
                    width: UILayout.medium,
                  ),
                ),
                UISpacing.spacingH16,
                Text(
                  'Lo antes posible',
                  style: bodyMediumRegular.copyWith(
                    color: Theme.of(context).colorScheme.onPrimary,
                  ),
                ),
              ],
            ),
            UISpacing.spacingV8,
            Row(
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Theme.of(context).colorScheme.onPrimary,
                    ),
                  ),
                  child: const SizedBox(
                    height: UILayout.medium,
                    width: UILayout.medium,
                  ),
                ),
                UISpacing.spacingH16,
                Text(
                  'Agendar una entrega',
                  style: bodyMediumRegular.copyWith(
                    color: Theme.of(context).colorScheme.onPrimary,
                  ),
                ),
                UISpacing.spacingH24,
                Icon(
                  Icons.edit_calendar_outlined,
                  size: UILayout.large,
                  color: Theme.of(context).colorScheme.onPrimary,
                ),
                UISpacing.spacingH4,
                Expanded(
                  child: TextField(
                    controller: TextEditingController(),
                    decoration: InputDecoration(
                      focusedBorder: const UnderlineInputBorder(),
                      focusColor: Theme.of(context).colorScheme.onPrimary,
                    ),
                    style: bodyXSmallBold.copyWith(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ],
            ),
            UISpacing.spacingV16,
            Text(
              'Método de pago: ',
              style: bodySmallItalic.copyWith(
                color: Theme.of(context).primaryColor,
              ),
            ),
            UISpacing.spacingV8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        DecoratedBox(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          child: const SizedBox(
                            height: UILayout.medium,
                            width: UILayout.medium,
                          ),
                        ),
                        UISpacing.spacingH16,
                        Text(
                          'Efectivo',
                          style: bodyMediumRegular.copyWith(
                            color: Theme.of(context).colorScheme.onPrimary,
                          ),
                        ),
                        UISpacing.spacingH8,
                        Icon(
                          Icons.payments_outlined,
                          color: Theme.of(context).colorScheme.onPrimary,
                          size: UILayout.large,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        DecoratedBox(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          child: const SizedBox(
                            height: UILayout.medium,
                            width: UILayout.medium,
                          ),
                        ),
                        UISpacing.spacingH16,
                        Text(
                          'Debito *4754',
                          style: bodyMediumRegular.copyWith(
                            color: Theme.of(context).colorScheme.onPrimary,
                          ),
                        ),
                        UISpacing.spacingH8,
                        Icon(
                          Icons.credit_card_rounded,
                          color: Theme.of(context).colorScheme.onPrimary,
                          size: UILayout.large,
                        ),
                      ],
                    ),
                  ],
                ),
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4)),
                    primary: Theme.of(context).primaryColor,
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.add_card_rounded,
                        color: Theme.of(context).colorScheme.onPrimary,
                        size: UILayout.large,
                      ),
                      Text(
                        'Añadir nuevo\nmétodo de pago',
                        style: bodySmallItalic.copyWith(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                        textAlign: TextAlign.right,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            UISpacing.spacingV16,
            Text(
              'Ingredientes: ',
              style: bodySmallItalic.copyWith(
                color: Theme.of(context).primaryColor,
              ),
            ),
            UISpacing.spacingV8,
            const QuantityRowWidget(
              title: 'cebolla',
              value: 1,
            ),
            const QuantityRowWidget(
              title: 'pimentón',
              value: 1,
            ),
            const QuantityRowWidget(
              title: 'sal',
              value: 100,
              unit: 'gr',
            ),
            const QuantityRowWidget(
              title: 'azucar',
              value: 100,
              unit: 'gr',
            ),
            const QuantityRowWidget(
              title: 'arroz',
              value: 100,
              unit: 'gr',
            ),
            const QuantityRowWidget(
              title: 'comino',
              value: 1,
            ),
            const QuantityRowWidget(
              title: 'tomates maduros',
              value: 1,
            ),
            UISpacing.spacingV16,
            Text(
              'Detalles de tu orden: ',
              style: bodySmallItalic.copyWith(
                color: Theme.of(context).primaryColor,
              ),
            ),
            UISpacing.spacingV8,
            DetailOrderRow(
              title: 'Productos',
              value: 82000,
            ),
            DetailOrderRow(
              title: 'Domicilio',
              value: 5000,
            ),
            DetailOrderRow(
              title: 'Servicio',
              value: 1500,
            ),
            DetailOrderRow(
              title: 'Total',
              value: 88500,
              colorText: UIColors.mediumSeaGreen,
            ),
            UISpacing.spacingV24,
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4)),
                    primary: Theme.of(context).primaryColor,
                  ),
                  child: Padding(
                    padding: UIPadding.paddingH4,
                    child: Text(
                      'REALIZAR PEDIDO',
                      style: bodySmallRegular.copyWith(
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class DetailOrderRow extends StatelessWidget {
  DetailOrderRow({
    required this.title,
    required this.value,
    this.colorText,
    Key? key,
  }) : super(key: key);

  final String title;
  final int value;
  final Color? colorText;

  final formatCurrency = NumberFormat.simpleCurrency();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: bodySmallRegular.copyWith(
            color: colorText ?? Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        Text(
          formatCurrency.format(value),
          style: bodySmallRegular.copyWith(
            color: colorText ?? Theme.of(context).colorScheme.onPrimary,
          ),
        ),
      ],
    );
  }
}

class QuantityRowWidget extends StatelessWidget {
  const QuantityRowWidget({
    required this.title,
    required this.value,
    this.unit,
    Key? key,
  }) : super(key: key);

  final String title;
  final int value;
  final String? unit;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          '- $title',
          style: bodySmallRegular.copyWith(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        QuantityWidget(value: value),
      ],
    );
  }
}

class QuantityWidget extends StatelessWidget {
  const QuantityWidget({
    required this.value,
    this.unit = '',
    Key? key,
  }) : super(key: key);

  final int value;
  final String? unit;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: radiusl4,
            color: Theme.of(context).primaryColor,
          ),
          child: Center(
            child: Icon(
              Icons.remove,
              size: UILayout.medium,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
          ),
        ),
        DecoratedBox(
          decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(context).colorScheme.onPrimary,
            ),
          ),
          child: SizedBox(
            width: UILayout.xlarge,
            child: Center(
              child: Text(
                '$value $unit',
                style: bodySmallRegular.copyWith(
                  color: Theme.of(context).colorScheme.onPrimary,
                ),
              ),
            ),
          ),
        ),
        DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: radiusr4,
            color: Theme.of(context).primaryColor,
          ),
          child: Center(
            child: Icon(
              Icons.add,
              size: UILayout.medium,
              color: Theme.of(context).colorScheme.onPrimary,
            ),
          ),
        ),
      ],
    );
  }
}
