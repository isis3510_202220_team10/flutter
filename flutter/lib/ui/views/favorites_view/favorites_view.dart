part of ui.views.screens;

class FavoritesListView extends StatelessWidget {
  FavoritesListView({Key? key}) : super(key: key);
  final RecipesService _recipesService = RecipesService();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FavoritesViewModel>.reactive(
      viewModelBuilder: FavoritesViewModel.new,
      onModelReady: (model) => model.initState(),
      onDispose: (model) => model.dispose(),
      builder: (
        BuildContext context,
        FavoritesViewModel model,
        _,
      ) =>
          Scaffold(
        appBar: AppBar(
          backgroundColor: UIColors.middleRed,
          toolbarHeight: UILayout.xsmall,
          elevation: 0.0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                flex: 18,
                child: model.favorites.isEmpty
                    ? ConnectionErrorWidget(
                        onRetry: _recipesService.getRecipes,
                        errmsg: 'No tienes recetas favoritas todavía',
                      )
                    : ListView.builder(
                        itemBuilder: (context, index) => RecipeSummary(
                          recipe: model.favorites[index],
                          key: UniqueKey(),
                        ),
                        itemCount: model.favorites.length,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
