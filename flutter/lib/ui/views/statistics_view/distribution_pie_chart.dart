part of ui.views.screens;

class DistributionPieChart extends StatefulWidget {
  const DistributionPieChart({
    required this.data,
    required this.colors,
    Key? key,
  }) : super(key: key);

  final Map<String, dynamic> data;
  final List<Color> colors;

  @override
  State<DistributionPieChart> createState() => DistributionPieChartState();
}

class DistributionPieChartState extends State<DistributionPieChart> {
  List<String> months = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ];
  NumberFormat formatCurrency = NumberFormat.simpleCurrency();
  NumberFormat formatCurrencyWithoutDecimals =
      NumberFormat.simpleCurrency(decimalDigits: 0);

  @override
  Widget build(BuildContext context) => Column(
        children: [
          SizedBox(
            width: 240,
            height: 240,
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                PieChart(
                  PieChartData(
                    borderData: FlBorderData(
                      show: false,
                    ),
                    centerSpaceRadius: 108,
                    sections: getSections(widget.data, widget.colors),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Total: ',
                      style: h1.copyWith(
                        color: UIColors.blackCoffee,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      formatCurrencyWithoutDecimals
                          .format(widget.data.values.last),
                      style: h1.copyWith(
                        color: UIColors.blackCoffee,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          UISpacing.spacingV48,
          Padding(
            padding: UIPadding.paddingH16,
            child: ListView.separated(
              shrinkWrap: true,
              itemBuilder: (context, index) => Row(
                children: <Widget>[
                  SizedBox(
                    height: UILayout.medium,
                    width: UILayout.medium,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: widget.colors[index % widget.colors.length],
                        borderRadius: radius4,
                      ),
                    ),
                  ),
                  UISpacing.spacingH8,
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          months[
                              int.parse(widget.data.keys.elementAt(index)) - 1],
                          style: bodyMediumItalic.copyWith(
                            color: UIColors.blackCoffee,
                          ),
                        ),
                        Text(
                          formatCurrency
                              .format(widget.data.values.elementAt(index)),
                          style: bodyMediumItalic.copyWith(
                            color: UIColors.blackCoffee,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              separatorBuilder: (context, index) => UISpacing.spacingV8,
              itemCount: widget.data.length - 1,
            ),
          )
        ],
      );

  List<PieChartSectionData> getSections(
    Map<String, dynamic> data,
    List<Color> colors,
  ) {
    return List<PieChartSectionData>.generate(
      data.length - 1,
      (int index) {
        return PieChartSectionData(
          color: colors[index % colors.length],
          value: double.tryParse('${data.values.elementAt(index)}'),
          radius: UILayout.xlarge,
          showTitle: false,
        );
      },
    );
  }
}
