part of ui.views.screens;

class StatisticsView extends StatelessWidget {
  const StatisticsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder.reactive(
      viewModelBuilder: StatisticsViewModel.new,
      onModelReady: (StatisticsViewModel model) {
        model.requestData();
      },
      builder: (BuildContext context, StatisticsViewModel model, _) => Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Center(
            child: Text(
              'Perfil',
              style: h1b.copyWith(
                color: Theme.of(context).colorScheme.onPrimary,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            if (model.isBusy) {
              return const Center(
                child: CircularProgressIndicator(
                  backgroundColor: UIColors.blackCoffee,
                ),
              );
            }
            if (model.hasError) {
              Object? error = model.modelError;
              return Stack(children: [
                ConnectionErrorWidget(
                  onRetry: () => model.notifyListeners(),
                  errmsg: ErrorHelper.getErrorMessage(
                    error,
                  ),
                ),
                Positioned.fill(
                  bottom: UILayout.medium,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                      onPressed: () {
                        Provider.of<Auth>(context, listen: false).logout();
                      },
                      style: ElevatedButton.styleFrom(
                        primary:
                            Theme.of(context).primaryColor.withOpacity(0.8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 10.0,
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'Cerrar sesión',
                            style: h4.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          const Icon(Icons.exit_to_app),
                        ],
                      ),
                    ),
                  ),
                ),
              ]);
            }
            if (model.caloricData.isEmpty && model.finantialData.isEmpty) {
              return Stack(children: [
                Center(
                  child: Text(
                    'Aún no has consumido nada por lo que no hay información para mostrar',
                    style: h0.copyWith(
                      color: UIColors.blackCoffee,
                    ),
                  ),
                ),
                Positioned.fill(
                  bottom: UILayout.medium,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                      onPressed: () {
                        Provider.of<Auth>(context, listen: false).logout();
                      },
                      style: ElevatedButton.styleFrom(
                        primary:
                            Theme.of(context).primaryColor.withOpacity(0.8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 10.0,
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'Cerrar sesión',
                            style: h4.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          const Icon(Icons.exit_to_app),
                        ],
                      ),
                    ),
                  ),
                ),
              ]);
            }
            return Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        'Calorias consumidas semanalmente en el último mes',
                        textAlign: TextAlign.center,
                        style: h5,
                      ),
                      UISpacing.spacingV8,
                      Text(
                        'Total: ${model.getTotalCaloricIntake(model.caloricData)}',
                        textAlign: TextAlign.center,
                        style: h5,
                      ),
                      SizedBox(
                        height: 400,
                        child: DChartBar(
                          data: [
                            {
                              'id': 'Bar',
                              'data': model.getJsonList(model.caloricData),
                            },
                          ],
                          domainLabelPaddingToAxisLine: 16,
                          axisLineTick: 2,
                          axisLinePointTick: 2,
                          axisLinePointWidth: 10,
                          axisLineColor: UIColors.blackCoffee,
                          measureLabelPaddingToAxisLine: 16,
                          barColor: (barData, index, id) => UIColors.middleRed,
                          showBarValue: true,
                          barValue: (barData, index) =>
                              barData['measure'].toString(),
                          xAxisTitle: 'Semanas',
                          yAxisTitle: 'Calorias',
                        ),
                      ),
                      UISpacing.spacingV48,
                      DistributionPieChart(
                        data: model.finantialData,
                        colors: model.pieChartColors,
                      ),
                      const SizedBox(
                        height: UILayout.imageSize,
                      ),
                    ],
                  ),
                ),
                Positioned.fill(
                  bottom: UILayout.medium,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                      onPressed: () {
                        Provider.of<Auth>(context, listen: false).logout();
                      },
                      style: ElevatedButton.styleFrom(
                        primary:
                            Theme.of(context).primaryColor.withOpacity(0.8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 10.0,
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'Cerrar sesión',
                            style: h4.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          const Icon(Icons.exit_to_app),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
