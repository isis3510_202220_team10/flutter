part of ui.views.screens;

class RecipeSummary extends StatelessWidget {
  const RecipeSummary({
    Key? key,
    required this.recipe,
  }) : super(key: key);
  final RecipeModel recipe;

  @override
  Widget build(BuildContext context) {
    //General card
    return InkWell(
      onTap: () => Navigator.pushNamed(
        context,
        RecipeDetail.routeName,
        arguments: recipe,
      ),
      child: Card(
        color: UIColors.eggshell,
        elevation: UILayout.small,
        shape: RoundedRectangleBorder(
          borderRadius: radius16,
          side: border2.copyWith(
            color: UIColors.blackCoffee,
            style: BorderStyle.solid,
          ),
        ),
        child: Row(
          //Image part
          children: [
            Card(
              clipBehavior: Clip.hardEdge,
              elevation: UILayout.xsmall,
              shape: RoundedRectangleBorder(
                borderRadius: radius16,
              ),
              child: CustomCachedNetworkImage(
                images: recipe.images,
              ),
            ),
            //Info of the recipe
            recipeInfo(
              context,
            ),
          ],
        ),
      ),
    );
  }

  //Second column of the card
  Widget recipeInfo(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width * 0.6,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Title
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  recipe.name,
                  style: bodyMediumBold.copyWith(
                    color: UIColors.middleRed,
                  ),
                  overflow: TextOverflow.clip,
                ),
              ),
              Row(
                children: [
                  Text(
                    recipe.rate.toString(),
                    style: bodySmallBold,
                  ),
                  const Icon(
                    Icons.star,
                    color: Colors.yellow,
                    shadows: [
                      Shadow(
                        color: UIColors.blackCoffee,
                        blurRadius: 10.0,
                      ),
                    ],
                  )
                ],
              )
            ],
          ),
          //Horizontal line that divides the title from the rest
          const Divider(
            thickness: 4,
            color: UIColors.blackCoffee,
          ),
          //Relevant information like duration of the recipe
          Container(
            margin: const EdgeInsets.only(
              bottom: UILayout.small,
            ),
            child: Row(
              children: [
                const Icon(
                  Icons.watch_later_outlined,
                  color: UIColors.middleRed,
                ),
                Text(
                  getDuration(recipe),
                  style: bodyMediumRegular,
                )
              ],
            ),
          ),
          //Recipe's description
          Text(
            recipe.description,
            maxLines: 6,
            overflow: TextOverflow.ellipsis,
            softWrap: true,
            textAlign: TextAlign.justify,
            style: bodySmallRegular,
          ),
        ],
      ),
    );
  }

  String getDuration(RecipeModel recipe) => recipe.duration >= 960
      ? '16h+'
      : recipe.duration >= 60
          ? '${(recipe.duration / 60).floor()}H:${recipe.duration % 60}m '
          : '${recipe.duration}m';
}
