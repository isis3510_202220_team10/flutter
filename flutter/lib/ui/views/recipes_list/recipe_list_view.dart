part of ui.views.screens;

class RecipeList extends StatelessWidget {
  const RecipeList({Key? key}) : super(key: key);
  static const routeName = '/recipe_list';
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RecipesListViewModel>.reactive(
      onModelReady: (model) => model.initState(context),
      viewModelBuilder: () => RecipesListViewModel(),
      builder: (context, model, _) => Scaffold(
        appBar: AppBar(
          backgroundColor: UIColors.middleRed,
          toolbarHeight: UILayout.xsmall,
          elevation: 0.0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                height: UILayout.xlarge + UILayout.mlarge,
                padding: const EdgeInsets.only(
                  top: UILayout.small,
                ),
                color: UIColors.middleRed,
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: TextButton(
                        onPressed: () => showDialog(
                          context: context,
                          builder: (context) => const SortDialog(),
                        ).then(
                          (value) => model.onRetry(),
                        ),
                        child: Container(
                          width: UILayout.xlarge,
                          height: UILayout.large,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              90,
                            ),
                            border: Border.all(
                              color: UIColors.blackCoffee,
                            ),
                          ),
                          child: const Icon(
                            Icons.filter_list,
                            color: UIColors.blackCoffee,
                          ),
                        ),
                      ),
                    ),
                    UISpacing.spacingH4,
                    Expanded(
                      flex: 9,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                            90,
                          ),
                        ),
                        clipBehavior: Clip.hardEdge,
                        child: TextFormField(
                          controller: model.textEditingController,
                          decoration: InputDecoration(
                            fillColor: UIColors.eggshell,
                            filled: true,
                            labelStyle: h4.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                            suffixIcon: Icon(
                              Icons.search,
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 18,
                child: (() {
                  if (model.loading && model.recipesDisplay.isEmpty) {
                    return const Center(
                      child: CircularProgressIndicator(
                        color: UIColors.blackCoffee,
                      ),
                    );
                  } else if (model.errorMessage != null) {
                    return ConnectionErrorWidget(
                      onRetry: model.onRetry,
                      errmsg: model.errorMessage!,
                    );
                  } else if (model.recipesDisplay.isEmpty) {
                    return Center(
                      child: Column(
                        children: [
                          Text(
                            'No tenemos recetas para mostrarte :(',
                            textAlign: TextAlign.center,
                            style: h0.copyWith(
                              color: UIColors.blackCoffee,
                            ),
                          ),
                          TextButton(
                            onPressed: model.onRetry,
                            child: Card(
                              color: UIColors.middleRed,
                              elevation: UILayout.xsmall,
                              shape: RoundedRectangleBorder(
                                borderRadius: radius4,
                              ),
                              child: Padding(
                                padding: UIPadding.padding_8,
                                child: Text(
                                  'Reintentar',
                                  style: bodyLargeBold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return RefreshIndicator(
                      onRefresh: () async {
                        model.onRetry();
                      },
                      child: Column(
                        children: [
                          Expanded(
                            child: ListView.builder(
                              controller: model.scrollController,
                              itemCount: model.recipesDisplay.length,
                              itemBuilder: (context, index) => RecipeSummary(
                                recipe: model.recipesDisplay[index],
                              ),
                            ),
                          ),
                          if (model.loading)
                            Card(
                              color: UIColors.eggshell,
                              elevation: UILayout.small,
                              shape: RoundedRectangleBorder(
                                borderRadius: radius16,
                                side: border2.copyWith(
                                  color: UIColors.blackCoffee,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              child: ConstrainedBox(
                                constraints: const BoxConstraints(
                                  minHeight: UILayout.imageSize,
                                ),
                                child: const Center(
                                  child: CircularProgressIndicator(
                                    color: UIColors.blackCoffee,
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    );
                  }
                }()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
