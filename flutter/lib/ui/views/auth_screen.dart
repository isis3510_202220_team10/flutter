part of ui.views.screens;

enum AuthMode { Signup, Login }

class AuthScreen extends StatelessWidget {
  static const routeName = '/auth';

  bool agree = false;

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: deviceSize.height,
              width: deviceSize.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Flexible(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: UILayout.large,
                        horizontal: UILayout.xlarge,
                      ),
                      alignment: Alignment.centerLeft,
                      child: Center(
                        child: RichText(
                          text: TextSpan(
                            text: 'Bienvenido a ',
                            style: h0b.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: 'Mikhuna',
                                style: h0b.copyWith(
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    flex: deviceSize.width > 600 ? 3 : 2,
                    child: const AuthCard(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key? key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  final Map<String, dynamic> _authData = {
    'email': '',
    'password': '',
    'name': '',
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();
  final textController = TextEditingController();

  void _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(
          '¡Un Error ha ocurrido!',
          style: h4b.copyWith(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        content: Text(
          message,
          style: bodySmallRegular.copyWith(
            color: Theme.of(context).colorScheme.onPrimary,
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              'Okay',
              style: bodySmallRegular.copyWith(
                color: Theme.of(context).primaryColor,
              ),
            ),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  Future<void> _submit() async {
    Connectivity connectivity = Connectivity();
    ConnectivityResult result = await connectivity.checkConnectivity();

    if (!_formKey.currentState!.validate()) {
      // Invalid!
      return;
    }

    _formKey.currentState?.save();
    setState(
      () {
        _isLoading = true;
      },
    );
    try {
      if (_authMode == AuthMode.Login) {
        // Log user in
        await Provider.of<Auth>(context, listen: false).login(
          _authData['email'],
          _authData['password'],
        );
      } else {
        // Sign user up
        await Provider.of<Auth>(context, listen: false).signup(
          _authData['email'],
          _authData['password'],
          _authData['name'],
        );
      }
    } on HttpException catch (error) {
      var errorMessage = 'Autenticación fallida';
      if (error.toString().contains('EMAIL_EXISTS')) {
        errorMessage = 'Esta dirección de correo electrónico ya está en uso.';
      } else if (error.toString().contains('INVALID_EMAIL')) {
        errorMessage = 'Esta no es una dirección de correo electrónico válida';
      } else if (error.toString().contains('WEAK_PASSWORD')) {
        errorMessage = 'Esta contraseña es demasiado débil.';
      } else if (error.toString().contains('EMAIL_NOT_FOUND')) {
        errorMessage =
            'No se pudo encontrar un usuario con ese correo electrónico.';
      } else if (error.toString().contains('INVALID_PASSWORD')) {
        errorMessage = 'Contraseña invalida.';
      }
      _showErrorDialog(errorMessage);
    } catch (error) {
      var errorMessage =
          'No se pudo autenticar. Por favor, inténtelo de nuevo más tarde.';
      if (result == ConnectivityResult.none) {
        errorMessage =
            'No tienes internet, por lo tanto no puedes iniciar sesion, vuelve a intentar mas tarde';
      }
      _showErrorDialog(errorMessage);
    }

    setState(
      () {
        _isLoading = false;
      },
    );
  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(
        () {
          _authMode = AuthMode.Signup;
        },
      );
    } else {
      setState(
        () {
          _authMode = AuthMode.Login;
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    var agree = false;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: _authMode == AuthMode.Signup ? 200 : 310,
              constraints: BoxConstraints(
                minHeight: _authMode == AuthMode.Signup ? 500 : 350,
              ),
              width: deviceSize.width,
              padding: const EdgeInsets.symmetric(
                  vertical: UILayout.xsmall, horizontal: UILayout.mlarge),
              alignment: Alignment.centerLeft,
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Correo Electrónico',
                          labelStyle: h4.copyWith(
                            color: Theme.of(context).primaryColor,
                          ),
                          prefixIcon: Icon(
                            Icons.email_outlined,
                            color: Theme.of(context).colorScheme.onPrimary,
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(25),
                        ],
                        validator: _authMode == AuthMode.Login
                            ? (value) {
                                if (value!.isEmpty || !value.contains('@')) {
                                  return 'Correo Invalido';
                                }
                                return null;
                              }
                            : null,
                        onSaved: (value) {
                          _authData['email'] = value!;
                        },
                      ),
                      if (_authMode == AuthMode.Login)
                        TextFormField(
                          enabled: _authMode == AuthMode.Login,
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            labelStyle: h4.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                            prefixIcon: Icon(
                              Icons.lock_outline,
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          obscureText: true,
                          controller: _passwordController,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(12),
                          ],
                          validator: _authMode == AuthMode.Login
                              ? (value) {
                                  if (value!.isEmpty || value.length < 5) {
                                    return 'La contraseña es muy corta';
                                  }
                                }
                              : null,
                          onSaved: (value) {
                            _authData['password'] = value!;
                          },
                        ),
                      if (_authMode == AuthMode.Signup)
                        TextFormField(
                          enabled: _authMode == AuthMode.Signup,
                          decoration: InputDecoration(
                            labelText: 'Nombre de Usuario',
                            labelStyle: h4.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                            prefixIcon: Icon(
                              Icons.person_outline,
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          keyboardType: TextInputType.name,
                          controller: textController,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                              RegExp(r"[a-zA-Z]+|\s"),
                            ),
                            LengthLimitingTextInputFormatter(15),
                          ],
                          validator: _authMode == AuthMode.Signup
                              ? (value) {
                                  if (value!.isEmpty) {
                                    return 'El nombre de usuario esta vacio';
                                  }
                                }
                              : null,
                          onSaved: (value) {
                            _authData['name'] = value!;
                          },
                        ),
                      if (_authMode == AuthMode.Signup)
                        TextFormField(
                          enabled: _authMode == AuthMode.Signup,
                          decoration: InputDecoration(
                            labelText: 'Contraseña',
                            labelStyle: h4.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                            prefixIcon: Icon(
                              Icons.lock_outline,
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          obscureText: true,
                          controller: _passwordController,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(12),
                          ],
                          validator: _authMode == AuthMode.Signup
                              ? (value) {
                                  if (value!.isEmpty || value.length < 5) {
                                    return 'La contraseña es muy corta';
                                  }
                                }
                              : null,
                          onSaved: (value) {
                            _authData['password'] = value!;
                          },
                        ),
                      if (_authMode == AuthMode.Signup)
                        TextFormField(
                          enabled: _authMode == AuthMode.Signup,
                          decoration: InputDecoration(
                            labelText: 'Confirmar Contraseña',
                            labelStyle: h4.copyWith(
                              color: Theme.of(context).primaryColor,
                            ),
                            prefixIcon: Icon(
                              Icons.lock_outline,
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                          obscureText: true,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(12),
                          ],
                          validator: _authMode == AuthMode.Signup
                              ? (value) {
                                  if (value != _passwordController.text) {
                                    return 'Las contraseñas no coinciden';
                                  }
                                }
                              : null,
                        ),
                      UISpacing.spacingV24,
                      if (_isLoading)
                        const CircularProgressIndicator()
                      else
                        ElevatedButton(
                          onPressed: _submit,
                          style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).primaryColor,
                            // textColor: Theme.of(context).colorScheme.onPrimary,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 80.0, vertical: 20.0),
                          ),
                          child: Text(
                            _authMode == AuthMode.Login
                                ? 'Entrar'
                                : 'Crear Cuenta',
                            style: h4.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                        ),
                      TextButton(
                        onPressed: _switchAuthMode,
                        style: TextButton.styleFrom(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 10),
                            tapTargetSize: MaterialTapTargetSize.shrinkWrap),
                        child: Text(
                          _authMode == AuthMode.Login
                              ? '¿No tienes una cuenta aún? Regístrate'
                              : '¿Ya tienes una cuenta? Entra a Mikhuna',
                          style: bodySmallRegular.copyWith(
                            color: Theme.of(context).colorScheme.onPrimary,
                          ),
                        ),
                      ),
                      // Button to test crashlytics to work
                      // TextButton(
                      //     onPressed: () => throw Exception(),
                      //     child: const Text("Throw Test Exception"),
                      // ),
                      if (_authMode == AuthMode.Signup)
                        CheckboxListTile(
                            title: Center(
                              child: RichText(
                                text: TextSpan(
                                  text: 'Acepto todos los ',
                                  style: bodySmallRegular.copyWith(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onPrimary),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: 'Términos y Condiciones',
                                      style: bodySmallRegular.copyWith(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .onPrimary),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            value: agree,
                            onChanged: (value) {
                              setState(() {
                                agree = value ?? false;
                              });
                            }),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
