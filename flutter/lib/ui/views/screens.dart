library ui.views.screens;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:d_chart/d_chart.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter/services.dart';

import 'package:intl/intl.dart';
import 'package:main_app/helpers/error_helper.dart';
import 'package:main_app/models/bar_chart_data_model.dart';
import 'package:main_app/models/ingredient_model.dart';

import 'package:main_app/providers/auth.dart';
import 'package:main_app/services/recipes_service.dart';
import 'package:main_app/ui/views/main_view/main_view_view_model.dart';
import 'package:main_app/ui/views/shopping_list/shopping_list_item.dart';
import 'package:main_app/ui/widgets/connection_error_widget.dart';
import 'package:main_app/ui/widgets/custom_cached_network_image.dart';
import 'package:main_app/ui/widgets/sort_dialog.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

import 'package:main_app/models/recipe_model.dart';
import 'package:main_app/ui/uiModels/custom_bottom_navigation_bar_item.dart';
import 'package:main_app/ui/views/main_view/custom_bottom_navigation_bar.dart';
import 'package:ui_shared/ui_shared.dart';

import '../../helpers/complex_text_helper.dart';
import '../../models/http_exception.dart';
import 'package:searchfield/searchfield.dart';

import '../models/base_view.dart';

part 'add_recipe/add_recipe_screen.dart';
part 'add_recipe/modal_image_picker.dart';
part 'add_recipe/text_field_param_recipe.dart';
part 'add_recipe/dropdown_param_recipe.dart';
part 'add_recipe/modal_no_network_add.dart';
part 'add_recipe/modal_load_anyway.dart';
part 'add_recipe/modal_succesful_add.dart';
part 'add_recipe/edit_recipe_screen.dart';
part './auth_screen.dart';
part './shopping_card_screen.dart';
part './recipe_detail/recipe_detail_view.dart';
part './main_view/main_view.dart';
part './recipes_list/recipe_summary.dart';
part './recipes_list/recipe_list_view.dart';
part './statistics_view/statistics_view.dart';
part './statistics_view/distribution_pie_chart.dart';
part './favorites_view/favorites_view.dart';
part './main_view/custom_view_favorites.dart';
part './recomendations_view/recomendations_view.dart';
part './shopping_list/shopping_list_view.dart';
