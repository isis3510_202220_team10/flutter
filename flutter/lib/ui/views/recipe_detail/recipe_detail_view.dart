part of ui.views.screens;

class RecipeDetail extends StatelessWidget {
  static const routeName = '/recipe_details';
  const RecipeDetail({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final RecipeModel recipe =
        ModalRoute.of(context)!.settings.arguments as RecipeModel;
    return ViewModelBuilder<RecipeDetailViewModel>.reactive(
      viewModelBuilder: RecipeDetailViewModel.new,
      onModelReady: (RecipeDetailViewModel model) {
        model.initState(recipe);
        model.canEditRecipe(recipe.userId ?? '');
      },
      builder: (
        BuildContext context,
        RecipeDetailViewModel model,
        _,
      ) =>
          Scaffold(
        appBar: AppBar(
          title: Text(
            recipe.name,
          ),
          actions: <Widget>[
            Padding(
              padding: UIPadding.paddingH32,
              child: Row(
                children: [
                  if (model.canEdit)
                    GestureDetector(
                      onTap: () => Navigator.pushNamed(
                        context,
                        EditRecipeScreen.routeName,
                        arguments: recipe,
                      ),
                      child: const Icon(
                        Icons.edit,
                        size: UILayout.large,
                      ),
                    ),
                  UISpacing.spacingH8,
                  GestureDetector(
                    onTap: () => model.toggleFavorite(recipe, context),
                    child: Icon(
                      recipe.favorite ? Icons.favorite : Icons.favorite_border,
                      size: UILayout.large,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => model.onAdd(context),
          child: const Icon(
            Icons.add,
            color: UIColors.blackCoffee,
          ),
        ),
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (recipe.images != null && recipe.images!.isNotEmpty)
                CachedNetworkImage(
                  imageUrl: recipe.images![0],
                  height: UILayout.imageSize * 1.5,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),
              UISpacing.spacingV8,
              Expanded(
                child: Padding(
                  padding: UIPadding.paddingH4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              const Icon(
                                Icons.watch_later_outlined,
                                color: UIColors.middleRed,
                              ),
                              Text(
                                model.getDuration(recipe),
                                style: bodyMediumRegular,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              const Icon(
                                Icons.attach_money_outlined,
                                color: UIColors.mediumSeaGreen,
                              ),
                              Text(
                                '~ 20K/Persona',
                                style: bodyMediumRegular,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                '${recipe.rate}',
                                style: bodyMediumBold,
                              ),
                              const Icon(
                                Icons.star,
                                color: Colors.yellow,
                                shadows: [
                                  Shadow(
                                    color: UIColors.blackCoffee,
                                    blurRadius: 10.0,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      const Divider(
                        color: UIColors.blackCoffee,
                        thickness: UILayout.xsmall,
                      ),
                      Expanded(
                        child: ListView(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Descripción',
                                  textAlign: TextAlign.start,
                                  style: bodyLargeBold.copyWith(
                                    color: UIColors.middleRed,
                                  ),
                                ),
                                Text(
                                  recipe.description,
                                  textAlign: TextAlign.justify,
                                  style: bodyMediumRegular.copyWith(
                                    color: UIColors.blackCoffee,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Ingredientes',
                                  textAlign: TextAlign.start,
                                  style: bodyLargeBold.copyWith(
                                    color: UIColors.middleRed,
                                  ),
                                ),
                                Text(
                                  'Max. 10 porciones',
                                  textAlign: TextAlign.start,
                                  style: bodySmallRegular.copyWith(
                                    color: UIColors.blackCoffee,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Porciones:',
                                      style: bodySmallBold.copyWith(
                                        color: UIColors.blackCoffee,
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        TextButton(
                                          style: TextButton.styleFrom(
                                            padding: const EdgeInsets.all(
                                              0,
                                            ),
                                            alignment: Alignment.center,
                                          ),
                                          onPressed: model.diners == 1
                                              ? null
                                              : () => model.decreaseDiners(
                                                  recipe.ingredients),
                                          child: Card(
                                            color: UIColors.eggshell,
                                            elevation: UILayout.small,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: radius16,
                                              side: border2.copyWith(
                                                color: UIColors.blackCoffee,
                                                style: BorderStyle.solid,
                                              ),
                                            ),
                                            child: Container(
                                              padding: UIPadding.paddingH16,
                                              margin: const EdgeInsets.all(
                                                0,
                                              ),
                                              child: Text(
                                                '-',
                                                style: bodyMediumBold.copyWith(
                                                  color: model.diners == 1
                                                      ? UIColors.blackCoffee
                                                      : UIColors.middleRed,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Text(
                                          '${model.diners}',
                                          style: bodySmallBold.copyWith(
                                            color: UIColors.blackCoffee,
                                          ),
                                        ),
                                        TextButton(
                                          style: TextButton.styleFrom(
                                            padding: const EdgeInsets.all(
                                              0,
                                            ),
                                            alignment: Alignment.center,
                                          ),
                                          onPressed: model.diners == 10
                                              ? null
                                              : () => model.increaseDiners(
                                                    recipe.ingredients,
                                                  ),
                                          child: Card(
                                            color: UIColors.eggshell,
                                            elevation: UILayout.small,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: radius16,
                                              side: border2.copyWith(
                                                color: UIColors.blackCoffee,
                                                style: BorderStyle.solid,
                                              ),
                                            ),
                                            child: Container(
                                              padding: UIPadding.paddingH16,
                                              margin: const EdgeInsets.all(
                                                0,
                                              ),
                                              child: Text(
                                                '+',
                                                style: bodyMediumBold.copyWith(
                                                  color: model.diners == 10
                                                      ? UIColors.blackCoffee
                                                      : UIColors.middleRed,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                if (model.loading)
                                  const CircularProgressIndicator(),
                                if (!model.loading)
                                  ...model.ingredients
                                      .map((e) => Text(
                                          '- ${ComplexTextHelper.getIngredientsUnits(e)}'))
                                      .toList(),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Preparación',
                                  textAlign: TextAlign.start,
                                  style: bodyLargeBold.copyWith(
                                    color: UIColors.middleRed,
                                  ),
                                ),
                                Text(
                                  recipe.process,
                                  textAlign: TextAlign.justify,
                                  style: bodyMediumRegular.copyWith(
                                    color: UIColors.blackCoffee,
                                  ),
                                ),
                              ],
                            ),
                            UISpacing.spacingV96,
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
