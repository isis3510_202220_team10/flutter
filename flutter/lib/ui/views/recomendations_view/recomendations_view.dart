part of ui.views.screens;

class RecomendationsListView extends StatelessWidget {
  RecomendationsListView({Key? key}) : super(key: key);
  final RecipesService _recipesService = RecipesService();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RecomendationsViewModel>.reactive(
      viewModelBuilder: RecomendationsViewModel.new,
      onModelReady: (model) => model.initState(),
      onDispose: (model) => model.dispose(),
      builder: (
        BuildContext context,
        RecomendationsViewModel model,
        _,
      ) =>
          Scaffold(
        appBar: AppBar(
          backgroundColor: UIColors.middleRed,
          toolbarHeight: UILayout.xsmall,
          elevation: 0.0,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                flex: 18,
                child: model.recomendations.isEmpty
                    ? ConnectionErrorWidget(
                        onRetry: _recipesService.getRecomendedRecipes,
                        errmsg: 'No tienes recetas recomendadas todavía',
                      )
                    : ListView.builder(
                        itemBuilder: (context, index) => RecipeSummary(
                          recipe: model.recomendations[index],
                          key: UniqueKey(),
                        ),
                        itemCount: model.recomendations.length,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
