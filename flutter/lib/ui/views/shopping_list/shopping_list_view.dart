part of ui.views.screens;

class ShoppingListView extends StatelessWidget {
  const ShoppingListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      ViewModelBuilder<ShoppingListViewModel>.reactive(
        viewModelBuilder: ShoppingListViewModel.new,
        onModelReady: (model) => model.initState(),
        onDispose: (model) => model.dispose(),
        builder: (context, model, _) => Scaffold(
          appBar: AppBar(
            backgroundColor: UIColors.middleRed,
            elevation: 0.0,
            title: Center(
              child: Text(
                'Lista de compras',
                style: h1b.copyWith(
                  color: Theme.of(context).colorScheme.onPrimary,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          body: SafeArea(
            child: Column(
              children: [
                Expanded(
                  flex: 18,
                  child: model.shoppingList.isEmpty
                      ? Center(
                          child: Text(
                            'No has agregado nada a tu lista de compras',
                            textAlign: TextAlign.center,
                            style: h0.copyWith(
                              color: UIColors.blackCoffee,
                            ),
                          ),
                        )
                      : ListView.builder(
                          itemCount: model.shoppingList.length,
                          itemBuilder: (context, index) => ShoppingListItem(
                            ingredient: model.shoppingList[index],
                            key: UniqueKey(),
                          ),
                        ),
                ),
              ],
            ),
          ),
        ),
      );
}
