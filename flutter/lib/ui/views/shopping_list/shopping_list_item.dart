import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:main_app/helpers/complex_text_helper.dart';
import 'package:main_app/models/ingredient_model.dart';
import 'package:main_app/ui/models/shopping_list_item_view_model.dart';
import 'package:stacked/stacked.dart';
import 'package:ui_shared/ui_shared.dart';

class ShoppingListItem extends StatelessWidget {
  final IngredientModel ingredient;
  const ShoppingListItem({Key? key, required this.ingredient})
      : super(key: key);

  @override
  Widget build(BuildContext context) =>
      ViewModelBuilder<ShoppingListItemViewModel>.reactive(
        onModelReady: (model) => model.initState(ingredient),
        viewModelBuilder: () => ShoppingListItemViewModel(),
        builder: (context, model, _) => Container(
          margin: const EdgeInsets.all(
            UILayout.xsmall,
          ),
          padding: UIPadding.padding_4,
          decoration: BoxDecoration(
            border: Border.all(
              color: UIColors.blackCoffee,
            ),
            borderRadius: radius8,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 2,
                child: Text(
                  ingredient.name,
                  style: bodyMediumRegular,
                ),
              ),
              Flexible(
                flex: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ingredient.quantity != null
                        ? TextButton(
                            style: TextButton.styleFrom(
                              padding: const EdgeInsets.all(
                                0,
                              ),
                              alignment: Alignment.center,
                            ),
                            onPressed: () =>
                                model.editShoppingListIngredient(context),
                            child: Card(
                              color: UIColors.eggshell,
                              elevation: UILayout.small,
                              shape: RoundedRectangleBorder(
                                borderRadius: radius16,
                                side: border2.copyWith(
                                  color: UIColors.blackCoffee,
                                  style: BorderStyle.solid,
                                ),
                              ),
                              child: Container(
                                height: UILayout.xlarge,
                                padding: UIPadding.paddingH16,
                                margin: const EdgeInsets.all(
                                  0,
                                ),
                                child: const Icon(
                                  Icons.save,
                                ),
                              ),
                            ),
                          )
                        : UISpacing.spacingH64,
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          if (ingredient.quantity != null)
                            Flexible(
                              fit: FlexFit.loose,
                              child: TextFormField(
                                controller: model.textController,
                                keyboardType:
                                    const TextInputType.numberWithOptions(
                                        decimal: true),
                                inputFormatters: [
                                  FilteringTextInputFormatter(
                                    RegExp(r'^\d+\.?\d{0,2}'),
                                    allow: true,
                                  ),
                                  LengthLimitingTextInputFormatter(
                                    6,
                                  ),
                                ],
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                  fillColor: UIColors.eggshell,
                                  filled: true,
                                  labelStyle: h4.copyWith(
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ),
                            ),
                          if (ingredient.unit != null)
                            Flexible(
                              fit: FlexFit.loose,
                              child: Text(
                                ComplexTextHelper.getUnitsShoppingList(
                                  ingredient,
                                ),
                                textAlign: TextAlign.center,
                                style: bodySmallBold.copyWith(
                                  color: UIColors.blackCoffee,
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(
                          0,
                        ),
                        alignment: Alignment.center,
                      ),
                      onPressed: () => model.deleteIngredientFromShoppingList(),
                      child: Card(
                        color: UIColors.eggshell,
                        elevation: UILayout.small,
                        shape: RoundedRectangleBorder(
                          borderRadius: radius16,
                          side: border2.copyWith(
                            color: UIColors.blackCoffee,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Container(
                          height: UILayout.xlarge,
                          padding: UIPadding.paddingH16,
                          margin: const EdgeInsets.all(
                            0,
                          ),
                          child: const Icon(
                            Icons.delete,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
}
