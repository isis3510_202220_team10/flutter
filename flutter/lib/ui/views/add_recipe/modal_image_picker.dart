part of ui.views.screens;

class ModalImagePicker extends StatelessWidget {
  const ModalImagePicker({
    Key? key,
    required this.onTapCamera,
    required this.onTapGallery,
  }) : super(key: key);

  final Future<void> Function() onTapCamera;
  final Future<void> Function() onTapGallery;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.15,
            vertical: size.height * 0.3,
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: UIColors.eggshell,
              borderRadius: radius4,
            ),
            child: Padding(
              padding: UIPadding.padding_8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.close_rounded,
                          color: UIColors.blackCoffee,
                          size: UILayout.large,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: UILayout.medium,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        SelectPickerButton(
                          icon: Icons.camera_alt_rounded,
                          onTapFcn: () {
                            onTapCamera();
                            Navigator.pop(context);
                          },
                          title: 'Cámara',
                        ),
                        SelectPickerButton(
                          icon: Icons.image_rounded,
                          onTapFcn: () {
                            onTapGallery();
                            Navigator.pop(context);
                          },
                          title: 'Galería',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class SelectPickerButton extends StatelessWidget {
  const SelectPickerButton({
    Key? key,
    required this.onTapFcn,
    required this.icon,
    required this.title,
    this.color,
  }) : super(key: key);

  final Function() onTapFcn;
  final IconData icon;
  final String title;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTapFcn,
      child: Card(
        color: UIColors.eggshell,
        elevation: UILayout.xsmall,
        shape: RoundedRectangleBorder(
          borderRadius: radius4,
        ),
        child: Padding(
          padding: UIPadding.padding_16,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                icon,
                size: UILayout.iconSize,
                color: color ?? Theme.of(context).primaryColor,
              ),
              UISpacing.spacingV4,
              Text(
                title,
                style: bodyMediumItalic.copyWith(
                  color: color ?? Theme.of(context).primaryColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
