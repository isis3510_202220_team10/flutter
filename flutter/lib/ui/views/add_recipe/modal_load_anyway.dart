part of ui.views.screens;

class ModalLoadAnyway extends StatelessWidget {
  const ModalLoadAnyway({
    Key? key,
    required this.loadFcn,
    this.isEditing = false,
  }) : super(key: key);

  final Future<void> Function(BuildContext) loadFcn;
  final bool isEditing;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.15,
            vertical: size.height * 0.15,
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: UIColors.eggshell,
              borderRadius: radius4,
            ),
            child: Padding(
              padding: UIPadding.padding_8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.close_rounded,
                          color: UIColors.blackCoffee,
                          size: UILayout.large,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.warning_amber_rounded,
                        color: Theme.of(context).errorColor,
                        size: UILayout.xlarge,
                      ),
                      UISpacing.spacingV8,
                      Text(
                        isEditing
                            ? 'Si actualizas tu receta sin internet, es muy probable que las nuevas imágenes que quieres añadir se pierdan en el proceso'
                            : 'Si añades tu receta sin internet, es muy probable que las imágenes que quieres añadir se pierdan en el proceso',
                        style: bodySmallBold.copyWith(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      UISpacing.spacingV8,
                      Text(
                        '¿Deseas continuar?',
                        style: bodySmallBold.copyWith(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomButton(
                            onPressedFcn: () {
                              Navigator.pop(context);
                            },
                            size: size,
                            primaryColor: UIColors.middleRed,
                            textBtn: 'No',
                          ),
                          UISpacing.spacingH8,
                          CustomButton(
                            onPressedFcn: () {
                              Navigator.pop(context);
                              loadFcn(context);
                            },
                            size: size,
                            primaryColor: UIColors.mediumSeaGreen,
                            textBtn: 'Si',
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
