part of ui.views.screens;

class ModalSuccessAdd extends StatelessWidget {
  const ModalSuccessAdd({
    Key? key,
    this.isEditing = false,
  }) : super(key: key);

  final bool isEditing;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.15,
            vertical: size.height * 0.15,
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: UIColors.eggshell,
              borderRadius: radius4,
            ),
            child: Padding(
              padding: UIPadding.padding_8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                          if (isEditing) {
                            Navigator.pop(context);
                          }
                        },
                        icon: const Icon(
                          Icons.close_rounded,
                          color: UIColors.blackCoffee,
                          size: UILayout.large,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.check_circle_outline,
                        color: UIColors.mediumSeaGreen,
                        size: UILayout.xlarge,
                      ),
                      UISpacing.spacingV8,
                      Text(
                        isEditing
                            ? 'Tu receta ha sido actualizada'
                            : 'Tu receta ha sido añadida',
                        style: bodySmallBold.copyWith(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                      ),
                      UISpacing.spacingV8,
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
