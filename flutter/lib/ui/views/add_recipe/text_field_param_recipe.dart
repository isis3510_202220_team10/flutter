part of ui.views.screens;

class TextFieldParamRecipe extends StatelessWidget {
  const TextFieldParamRecipe({
    required this.title,
    required this.controller,
    required this.errorPlainText,
    this.columnMode = true,
    this.style,
    this.type,
    this.onChanged,
    this.hintText,
    this.hintStyle,
    this.formatters,
    Key? key,
  }) : super(key: key);

  final Function(String)? onChanged;
  final String title;
  final TextEditingController controller;
  final TextStyle? style;
  final bool columnMode;
  final TextInputType? type;
  final String errorPlainText;
  final String? hintText;
  final TextStyle? hintStyle;
  final List<TextInputFormatter>? formatters;

  @override
  Widget build(BuildContext context) {
    return columnMode
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                style: style ??
                    bodyMediumItalic.copyWith(
                      color: Theme.of(context).colorScheme.onPrimary,
                    ),
              ),
              UISpacing.spacingV8,
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: TextField(
                  controller: controller,
                  keyboardType: type,
                  onChanged: onChanged,
                  inputFormatters: formatters,
                  decoration: InputDecoration(
                    errorText: errorPlainText,
                    errorMaxLines: 2,
                    focusedBorder: const UnderlineInputBorder(),
                    focusColor: Theme.of(context).primaryColor,
                    hintText: hintText,
                    hintStyle: hintStyle,
                  ),
                  maxLines: null,
                ),
              ),
            ],
          )
        : Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: style ??
                    bodyMediumItalic.copyWith(
                      color: Theme.of(context).colorScheme.onPrimary,
                    ),
              ),
              UISpacing.spacingH8,
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.5,
                child: TextField(
                  controller: controller,
                  keyboardType: type,
                  onChanged: onChanged,
                  inputFormatters: formatters,
                  decoration: InputDecoration(
                    errorText: errorPlainText,
                    errorMaxLines: 2,
                    focusedBorder: const UnderlineInputBorder(),
                    focusColor: Theme.of(context).primaryColor,
                    hintText: hintText,
                    hintStyle: hintStyle,
                  ),
                ),
              ),
            ],
          );
  }
}
