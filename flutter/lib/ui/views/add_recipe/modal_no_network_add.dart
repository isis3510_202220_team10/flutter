part of ui.views.screens;

class ModalNoNetworkToAdd extends StatelessWidget {
  const ModalNoNetworkToAdd({
    Key? key,
    required this.retryCheck,
    required this.loadAnyway,
    this.isEditing = false,
  }) : super(key: key);

  final Future<void> Function(BuildContext) retryCheck;
  final void Function(BuildContext) loadAnyway;
  final bool isEditing;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Material(
      color: Colors.transparent,
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.15,
            vertical: size.height * 0.3,
          ),
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: UIColors.eggshell,
              borderRadius: radius4,
            ),
            child: Padding(
              padding: UIPadding.padding_8,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.close_rounded,
                          color: UIColors.blackCoffee,
                          size: UILayout.large,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.wifi_off_rounded,
                        color: Theme.of(context).errorColor,
                        size: UILayout.xlarge,
                      ),
                      Text(
                        'No tienes internet',
                        style: bodyMediumBold.copyWith(
                          color: Theme.of(context).errorColor,
                        ),
                      ),
                      UISpacing.spacingV8,
                      CustomButton(
                        onPressedFcn: () {
                          Navigator.pop(context);
                          retryCheck(context);
                        },
                        size: size,
                        primaryColor: UIColors.mediumSeaGreen,
                        textBtn: 'Reintentar',
                      ),
                      UISpacing.spacingV4,
                      CustomButton(
                        onPressedFcn: () {
                          Navigator.pop(context);
                          loadAnyway(context);
                        },
                        size: size,
                        primaryColor: UIColors.middleRed,
                        textBtn: isEditing
                            ? 'Actualizar receta sin internet'
                            : 'Añadir receta sin internet',
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.onPressedFcn,
    required this.size,
    required this.primaryColor,
    required this.textBtn,
  }) : super(key: key);

  final void Function() onPressedFcn;
  final Size size;
  final Color primaryColor;
  final String textBtn;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressedFcn,
      style: ElevatedButton.styleFrom(
        elevation: UILayout.small,
        primary: primaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        maximumSize: Size(
          size.width * 0.7,
          size.height * 0.1,
        ),
      ),
      child: Text(
        textBtn,
        style: bodySmallItalic.copyWith(
          color: Theme.of(context).colorScheme.onPrimary,
        ),
      ),
    );
  }
}
