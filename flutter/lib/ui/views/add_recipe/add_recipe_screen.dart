part of ui.views.screens;

class AddRecipeScreen extends StatefulWidget {
  const AddRecipeScreen({Key? key}) : super(key: key);
  static const routeName = '/add_recipes';

  @override
  State<AddRecipeScreen> createState() => _AddRecipeScreenState();
}

class _AddRecipeScreenState extends State<AddRecipeScreen> {
  @override
  Widget build(BuildContext context) =>
      ViewModelBuilder<AddRecipeViewModel>.reactive(
        viewModelBuilder: AddRecipeViewModel.new,
        onModelReady: (AddRecipeViewModel viewModel) {
          viewModel
            ..getLostData()
            ..getIngredientsFromDB();
        },
        builder: (BuildContext context, AddRecipeViewModel viewModel, _) =>
            Scaffold(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          appBar: AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            title: Center(
              child: Text(
                'Añadir receta',
                style: h1b.copyWith(
                  color: Theme.of(context).colorScheme.onPrimary,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          body: Scrollbar(
            controller: viewModel.scrollController,
            thumbVisibility: true,
            radius: const Radius.circular(UILayout.medium),
            child: SingleChildScrollView(
              controller: viewModel.scrollController,
              physics: const ClampingScrollPhysics(),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: UILayout.mlarge,
                  vertical: UILayout.medium,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFieldParamRecipe(
                      title: 'Nombre *',
                      controller: viewModel.controllers['name']!['controller']
                              as TextEditingController? ??
                          TextEditingController(),
                      errorPlainText:
                          viewModel.controllers['name']!['error'] as String,
                      onChanged: (String value) {
                        viewModel.requestedField(value, 'name');
                      },
                      hintText: 'Ponle un nombre a tu receta',
                      hintStyle: bodySmallRegular,
                      formatters: <TextInputFormatter>[
                        LengthLimitingTextInputFormatter(20),
                        FilteringTextInputFormatter.allow(
                          RegExp('^[a-zA-Z0-9ñÑáéíóúü ]*'),
                        )
                      ],
                    ),
                    UISpacing.spacingV16,
                    TextFieldParamRecipe(
                      title: 'Descripción',
                      controller:
                          viewModel.controllers['description']!['controller']
                                  as TextEditingController? ??
                              TextEditingController(),
                      errorPlainText: viewModel
                          .controllers['description']!['error'] as String,
                      hintText: 'Ponle una descripción llamativa a tu receta',
                      hintStyle: bodySmallRegular,
                      formatters: <TextInputFormatter>[
                        LengthLimitingTextInputFormatter(125),
                        FilteringTextInputFormatter.allow(
                          RegExp('^[a-zA-Z0-9ñÑ.,;áéíóúü ]*'),
                        )
                      ],
                      type: TextInputType.multiline,
                    ),
                    UISpacing.spacingV16,
                    TextFieldParamRecipe(
                      title: 'Pasos *',
                      controller: viewModel.controllers['steps']!['controller']
                              as TextEditingController? ??
                          TextEditingController(),
                      errorPlainText:
                          viewModel.controllers['steps']!['error'] as String,
                      onChanged: (String value) {
                        viewModel.requestedField(value, 'steps');
                      },
                      hintText: 'Dinos los pasos para tu receta',
                      hintStyle: bodySmallRegular,
                      formatters: <TextInputFormatter>[
                        LengthLimitingTextInputFormatter(500),
                        FilteringTextInputFormatter.allow(
                          RegExp('^[a-zA-Z0-9ñÑáéíóúü.,;()\n ]*'),
                        )
                      ],
                      type: TextInputType.multiline,
                    ),
                    UISpacing.spacingV16,
                    Text(
                      'Imagen(es)*',
                      style: bodyMediumItalic.copyWith(
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                    ),
                    if (viewModel.images.isNotEmpty)
                      Text(
                        '''(límite 5 imágenes)\nPara eliminar una imagen dale dos toques''',
                        style: bodySmallItalic.copyWith(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    UISpacing.spacingV4,
                    if (viewModel.images.isNotEmpty)
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: UILayout.xlarge,
                        child: Row(
                          children: <Widget>[
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: MediaQuery.of(context).size.width -
                                    (UILayout.imageSize),
                              ),
                              child: ListView.separated(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemCount: viewModel.imagesLength,
                                itemBuilder:
                                    (BuildContext context, int index) =>
                                        InkWell(
                                  onDoubleTap: () =>
                                      viewModel.removeImage(index),
                                  child: Image.file(
                                    viewModel.images[index],
                                    width: UILayout.xlarge,
                                    height: UILayout.xlarge,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                separatorBuilder:
                                    (BuildContext context, int index) =>
                                        UISpacing.spacingH4,
                              ),
                            ),
                            UISpacing.spacingH4,
                            InkWell(
                              onTap: () => viewModel.showModal(
                                context,
                                ModalImagePicker(
                                  onTapCamera: viewModel.pickImageCamera,
                                  onTapGallery: viewModel.pickImageGallery,
                                ),
                                dismissible: false,
                              ),
                              child: SizedBox(
                                width: UILayout.xlarge,
                                height: UILayout.xlarge,
                                child: DecoratedBox(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.add_photo_alternate_outlined,
                                      size: UILayout.large,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    else
                      Text(
                        'Seleccione mínimo una imagen',
                        style: bodySmallItalic.copyWith(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                      ),
                    UISpacing.spacingV4,
                    if (viewModel.images.isEmpty)
                      InkWell(
                        onTap: () => viewModel.showModal(
                          context,
                          ModalImagePicker(
                            onTapCamera: viewModel.pickImageCamera,
                            onTapGallery: viewModel.pickImageGallery,
                          ),
                          dismissible: false,
                        ),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: UILayout.mlarge,
                              ),
                              child: Icon(
                                Icons.add_photo_alternate_outlined,
                                size: UILayout.xlarge,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                    if (viewModel.getErrorController('images').isNotEmpty)
                      Text(
                        viewModel.getErrorController('images'),
                        style: bodyXSmallRegular.copyWith(
                          color: Theme.of(context).errorColor,
                        ),
                      ),
                    UISpacing.spacingV16,
                    DropdownParamRecipe(
                      title: 'Ingredientes *',
                      fillWidth: true,
                      items: viewModel.ingredientsNames,
                      onChanged: viewModel.onChangeIngredients,
                      controller:
                          viewModel.controllers['ingredients']!['controller']
                              as TextEditingController,
                      focus: viewModel.controllers['ingredients']!['focus']
                          as FocusNode,
                      hintText: 'Escribe los ingredientes',
                      hintStyle: bodySmallRegular,
                    ),
                    if (viewModel.selectedIngredients.isNotEmpty)
                      ListView.builder(
                        itemCount: viewModel.selectedIngredients.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.15,
                                  child: TextField(
                                    controller: viewModel
                                        .getIngredientController(index, true),
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                      focusedBorder:
                                          const UnderlineInputBorder(),
                                      focusColor:
                                          Theme.of(context).primaryColor,
                                      hintText: 'Cantidad',
                                      hintStyle: bodyXSmallBold,
                                    ),
                                    onChanged: (String value) {
                                      viewModel.onChangedQuantity(value, index);
                                    },
                                    inputFormatters: <TextInputFormatter>[
                                      LengthLimitingTextInputFormatter(4),
                                      FilteringTextInputFormatter.allow(
                                        RegExp('^[0-9,/]*'),
                                      )
                                    ],
                                  ),
                                ),
                                UISpacing.spacingH4,
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.15,
                                  child: SearchField<String>(
                                    suggestions: viewModel.measumentsUnits
                                        .map((String e) =>
                                            SearchFieldListItem<String>(e))
                                        .toList(),
                                    suggestionState: Suggestion.hidden,
                                    textInputAction: TextInputAction.next,
                                    hasOverlay: false,
                                    searchStyle: bodySmallRegular.copyWith(
                                      color: UIColors.blackCoffee,
                                    ),
                                    searchInputDecoration: InputDecoration(
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                          color: Theme.of(context).primaryColor,
                                        ),
                                      ),
                                      hintText: 'Unidad',
                                      hintStyle: bodyXSmallBold,
                                    ),
                                    suggestionItemDecoration:
                                        const BoxDecoration(
                                      color: UIColors.eggshell,
                                    ),
                                    maxSuggestionsInViewPort: 6,
                                    itemHeight: 50,
                                    onSuggestionTap:
                                        (SearchFieldListItem<String> value) {
                                      viewModel.onChangedUnit(
                                        value.searchKey,
                                        index,
                                      );
                                    },
                                    focusNode:
                                        viewModel.getIngredientFocus(index),
                                    controller: viewModel
                                        .getIngredientController(index, false),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              viewModel.selectedIngredients[index],
                              style: bodySmallItalic.copyWith(
                                color: UIColors.blackCoffee,
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                viewModel.eraseSelectedIngredient(index);
                              },
                              icon: const Icon(
                                Icons.close_rounded,
                                size: UILayout.iconSizeSmall,
                                color: UIColors.blackCoffee,
                              ),
                              splashColor: Colors.transparent,
                            ),
                          ],
                        ),
                      ),
                    if (viewModel.getErrorController('ingredients').isNotEmpty)
                      Text(
                        viewModel.getErrorController('ingredients'),
                        style: bodyXSmallRegular.copyWith(
                          color: Theme.of(context).errorColor,
                        ),
                      ),
                    UISpacing.spacingV16,
                    TextFieldParamRecipe(
                      title: 'Tipo de comida',
                      controller: viewModel.controllers['type']!['controller']
                          as TextEditingController,
                      errorPlainText:
                          viewModel.controllers['type']!['error'] as String,
                      style: bodySmallItalic.copyWith(
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                      hintText: 'Escribe el o los tipos de esta comida',
                      hintStyle: bodySmallRegular,
                      formatters: <TextInputFormatter>[
                        LengthLimitingTextInputFormatter(100),
                        FilteringTextInputFormatter.allow(
                          RegExp('^[a-zA-Z0-9,;.ñÑáéíóúü ]*'),
                        )
                      ],
                    ),
                    UISpacing.spacingV16,
                    TextFieldParamRecipe(
                      title: 'Tiempo de preparación (minutos)*',
                      style: bodySmallItalic.copyWith(
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                      controller: viewModel.controllers['time']!['controller']
                              as TextEditingController? ??
                          TextEditingController(),
                      columnMode: true,
                      errorPlainText:
                          viewModel.controllers['time']!['error'] as String,
                      type: TextInputType.number,
                      onChanged: (String value) {
                        viewModel.requestedField(value, 'time');
                      },
                      hintText: 'Dinos el tiempo que tardaremos',
                      hintStyle: bodySmallRegular,
                      formatters: <TextInputFormatter>[
                        LengthLimitingTextInputFormatter(3),
                        FilteringTextInputFormatter.allow(RegExp('^[0-9]*'))
                      ],
                    ),
                    UISpacing.spacingV16,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: viewModel.cleanFields,
                          style: ElevatedButton.styleFrom(
                            side: BorderSide(
                              color: Theme.of(context).primaryColor,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: radius4,
                            ),
                            primary: Theme.of(context).colorScheme.background,
                          ),
                          child: Text(
                            'BORRAR',
                            style: bodySmallBold.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () => viewModel.checkConnectivity(context),
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: radius4,
                            ),
                            primary: Theme.of(context).primaryColor,
                          ),
                          child: Text(
                            'AÑADIR',
                            style: bodySmallBold.copyWith(
                              color: Theme.of(context).colorScheme.onPrimary,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
}
