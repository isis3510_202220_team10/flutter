part of ui.views.screens;

class DropdownParamRecipe extends StatelessWidget {
  const DropdownParamRecipe({
    required this.title,
    required this.fillWidth,
    required this.items,
    required this.onChanged,
    required this.controller,
    required this.focus,
    this.hintText,
    this.hintStyle,
    this.selectedItems,
    this.style,
    Key? key,
  }) : super(key: key);

  final String title;
  final bool fillWidth;
  final List<String> items;
  final List<String>? selectedItems;
  final Function(String) onChanged;
  final TextStyle? style;
  final TextEditingController controller;
  final FocusNode focus;
  final String? hintText;
  final TextStyle? hintStyle;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: style ??
              bodyMediumItalic.copyWith(
                color: Theme.of(context).colorScheme.onPrimary,
              ),
        ),
        UISpacing.spacingV8,
        SearchField(
          suggestions: items.map((e) => SearchFieldListItem(e)).toList(),
          suggestionState: Suggestion.hidden,
          textInputAction: TextInputAction.next,
          hasOverlay: false,
          searchStyle: bodySmallRegular.copyWith(
            color: UIColors.blackCoffee,
          ),
          searchInputDecoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).primaryColor,
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Theme.of(context).primaryColor,
              ),
            ),
            hintText: hintText,
            hintStyle: hintStyle,
          ),
          suggestionItemDecoration: const BoxDecoration(
            color: UIColors.eggshell,
          ),
          maxSuggestionsInViewPort: 6,
          itemHeight: 50,
          onSuggestionTap: (value) {
            onChanged(value.searchKey);
          },
          focusNode: focus,
          controller: controller,
        )
      ],
    );
  }
}