import 'package:flutter/material.dart';
import 'package:ui_shared/ui_shared.dart';

class CustomIcon extends StatelessWidget {
  final int iconIndex;
  final int currentIndex;
  final IconData iconData;
  final String label;
  final ValueChanged<int>? onTap;
  const CustomIcon({
    Key? key,
    required this.iconIndex,
    required this.currentIndex,
    required this.iconData,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap != null ? onTap!(iconIndex) : null,
      child: Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Container(
            height: UILayout.xlarge + UILayout.medium,
            padding: const EdgeInsets.only(
              top: UILayout.mlarge,
            ),
            child: Text(
              label,
              style: bodySmallRegular,
            ),
          ),
          iconIndex == currentIndex
              ? Positioned(
                  top: -UILayout.medium,
                  child: Container(
                    padding: UIPadding.padding_4,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: UIColors.eggshell,
                      border: Border.all(
                        color: UIColors.middleRed,
                      ),
                    ),
                    child: Icon(
                      iconData,
                      size: UILayout.mlarge,
                      color: UIColors.middleRed,
                    ),
                  ),
                )
              : Positioned(
                  top: UILayout.small,
                  child: Icon(
                    iconData,
                    size: UILayout.large,
                    color: UIColors.blackCoffee,
                  ),
                ),
        ],
      ),
    );
  }
}
