import 'dart:isolate';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:main_app/services/user_service.dart';
import 'package:main_app/ui/models/base_view.dart';
import 'package:main_app/ui/widgets/technological_expertise_dialog_view_model.dart';

import '../../../models/user_model.dart';
import '../../uiModels/custom_bottom_navigation_bar_item.dart';
import '../../widgets/technological_expertise_dialog.dart';
import '../screens.dart';

timeSkill(SendPort sendPort) async {
  final ReceivePort receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);
  Stopwatch stopwatch = Stopwatch()..start();
  bool saveTime = true;
  receivePort.listen((viewIndex) {
    if (viewIndex == 4 && saveTime) {
      saveTime = false;
      stopwatch.stop();
      int elapsedTime = stopwatch.elapsedMilliseconds;
      sendPort.send(elapsedTime);
    } else if (viewIndex == 0) {
      saveTime = true;
      stopwatch.reset();
      stopwatch.start();
    }
  });
}

class MainViewViewModel extends BaseViewModel {
  int currentIndex = 0;
  static final UserService userService = UserService();
  Isolate? isolate;
  ReceivePort? receivePort = ReceivePort();
  StreamQueue? recieveQueue;
  var isolateSendPort;

  void initState(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      UserModel user = userService.getUser()!;
      bool hasPassedEnoughTime = user.lastAsked == null ||
          DateTime.now().isAfter(user.lastAsked!.add(const Duration(days: 1)));
      if (user.technicalSkill == null && hasPassedEnoughTime) {
        showModal(context).then((value) {
          user = userService.getUser()!;
          startCounterIfTechnicalSkill(user);
        });
      } else {
        startCounterIfTechnicalSkill(user);
      }
    });
  }

  @override
  void dispose() {
    try {
      if (isolate != null) {
        isolate!.kill();
      }
      super.dispose();
    } catch (e) {}
  }

  void startCounterIfTechnicalSkill(UserModel user) async {
    if (user.technicalSkill != null) {
      receivePort = ReceivePort();
      recieveQueue = StreamQueue(receivePort!);
      isolate = await Isolate.spawn(timeSkill, receivePort!.sendPort);
      isolateSendPort = await recieveQueue!.next;
      isolateSendPort!.send(0);
      recieveQueue!.rest.listen((time) {
        userService.updateSkill(time);
      });
    }
  }

  Future<void> showModal(BuildContext context) => showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) => const TechnologicalExpertiseDialog(),
      );

  final List<CustomBottomNavigationBarItem> items = [
    CustomBottomNavigationBarItem(
      iconData: Icons.restaurant_menu_outlined,
      label: "Recetas",
    ),
    CustomBottomNavigationBarItem(
      iconData: Icons.favorite,
      label: "Para ti",
    ),
    CustomBottomNavigationBarItem(
      iconData: Icons.note_add_outlined,
      label: "Añadir receta",
    ),
    CustomBottomNavigationBarItem(
      iconData: Icons.person_outline_outlined,
      label: "Perfil",
    ),
    CustomBottomNavigationBarItem(
      iconData: Icons.list_alt_outlined,
      label: "Lista de compras",
    ),
  ];
  void updateCurrentIndex(int index) {
    currentIndex = index;
    if (isolateSendPort != null) {
      isolateSendPort!.send(currentIndex);
    }
    notifyListeners();
  }

  List<Widget> screens = [
    const RecipeList(),
    CustomViewFav(),
    const AddRecipeScreen(),
    const StatisticsView(),
    const ShoppingListView()
  ];
}
