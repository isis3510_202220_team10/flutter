import 'package:flutter/material.dart';
import 'package:main_app/ui/uiModels/custom_bottom_navigation_bar_item.dart';
import 'package:main_app/ui/views/main_view/custom_icon.dart';
import 'package:ui_shared/ui_shared.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  final Color? backgroundColor;
  final int currentIndex;
  final List<CustomBottomNavigationBarItem> items;
  final ValueChanged<int>? onTap;
  const CustomBottomNavigationBar({
    Key? key,
    this.backgroundColor,
    required this.currentIndex,
    required this.items,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor ?? Theme.of(context).primaryColor,
      ),
      width: size.width,
      height: UILayout.xlarge + UILayout.medium,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          for (var i = 0; i < items.length; i++)
            CustomIcon(
              iconIndex: i,
              currentIndex: currentIndex,
              iconData: items[i].iconData,
              label: items[i].label,
              onTap: (i) => onTap != null ? onTap!(i) : null,
            ),
        ],
      ),
    );
  }
}
