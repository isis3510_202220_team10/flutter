part of ui.views.screens;

class MainView extends StatelessWidget {
  static const routeName = '/main_view';
  const MainView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      ViewModelBuilder<MainViewViewModel>.reactive(
        onModelReady: (model) => model.initState(context),
        onDispose: (model) => model.dispose(),
        viewModelBuilder: () => MainViewViewModel(),
        builder: (context, model, _) => Scaffold(
          body: IndexedStack(
            index: model.currentIndex,
            children: model.screens,
          ),
          bottomNavigationBar: CustomBottomNavigationBar(
            currentIndex: model.currentIndex,
            onTap: (int index) => model.updateCurrentIndex(index),
            backgroundColor: UIColors.middleRed,
            items: model.items,
          ),
        ),
      );
}
