part of ui.views.screens;

class CustomViewFav extends StatefulWidget with ChangeNotifier {
  static const routeName = '/custom_view';
  CustomViewFav({Key? key}) : super(key: key);

  @override
  State<CustomViewFav> createState() => _CustomViewFavState();
}

class _CustomViewFavState extends State<CustomViewFav> {
  int _id_page = 0;
  final List<CustomBottomNavigationBarItem> _items = [
    CustomBottomNavigationBarItem(
      iconData: Icons.favorite,
      label: "Favoritas",
    ),
    CustomBottomNavigationBarItem(
      iconData: Icons.recommend,
      label: "Recomendations",
    ),
  ];

  @override
  void dispose() {
    super.dispose();
  }

  final List<Widget> _screens = [FavoritesListView(), RecomendationsListView()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          UISpacing.spacingV32,
          Container(
            width: MediaQuery.of(context).size.width,
            height: 70,
            color: Theme.of(context).primaryColor,
            child: Text(
              'Para ti',
              style: h1b.copyWith(
                color: Theme.of(context).colorScheme.onPrimary,
                backgroundColor: Theme.of(context).primaryColor,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          CustomBottomNavigationBar(
            currentIndex: _id_page,
            onTap: (int index) => setState(() {
              _id_page = index;
            }),
            backgroundColor: UIColors.middleRed,
            items: _items,
          ),
          Expanded(
            child: IndexedStack(
              index: _id_page,
              children: _screens,
            ),
          ),
        ],
      ),
    );
  }
}
