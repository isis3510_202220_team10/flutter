import 'package:flutter/material.dart';

class CustomBottomNavigationBarItem {
  IconData iconData;
  String label;

  CustomBottomNavigationBarItem({required this.iconData, required this.label});
}
