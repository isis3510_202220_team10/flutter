import 'package:flutter/material.dart';
import 'package:main_app/ui/widgets/technological_expertise_dialog_view_model.dart';
import 'package:stacked/stacked.dart';
import 'package:ui_shared/ui_shared.dart';

class TechnologicalExpertiseDialog extends StatelessWidget {
  const TechnologicalExpertiseDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<TechnologicalExpertiseDialogViewModel>.reactive(
      viewModelBuilder: () => TechnologicalExpertiseDialogViewModel(),
      builder: (BuildContext context,
              TechnologicalExpertiseDialogViewModel model, _) =>
          AlertDialog(
        title: Text(
          '¡Queremos mejorar tu experiencia en Mikhuna!',
          style: h4,
          textAlign: TextAlign.center,
        ),
        backgroundColor: UIColors.eggshell,
        content: ConstrainedBox(
          constraints: const BoxConstraints(maxHeight: 350),
          child: Column(
            children: (() {
              List<Widget> resp = [
                const Divider(),
                Text(
                  '¿Qué tanta experiencia consideras que tienes con la tecnología?',
                  style: bodyMediumRegular,
                  textAlign: TextAlign.justify,
                ),
              ];
              int selected = model.selected;
              String act;
              for (int i = 0;
                  i < TechnologicalExpertiseDialogViewModel.options.length;
                  i++) {
                act = TechnologicalExpertiseDialogViewModel.options[i];
                bool isSelected = i == selected;
                resp.add(
                  TextButton(
                    onPressed: () => model.selectTechnicalSkill(i),
                    child: Card(
                      color:
                          isSelected ? UIColors.middleRed : UIColors.eggshell,
                      elevation: UILayout.xsmall,
                      shape: RoundedRectangleBorder(
                        borderRadius: radius4,
                      ),
                      child: Padding(
                        padding: UIPadding.padding_8,
                        child: Text(
                          act,
                          style: bodyMediumRegular,
                        ),
                      ),
                    ),
                  ),
                );
              }
              resp.add(const Divider());
              resp.add(Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    onPressed: () => model.onCancel(context),
                    child: Card(
                      color: UIColors.middleRed,
                      elevation: UILayout.xsmall,
                      shape: RoundedRectangleBorder(
                        borderRadius: radius4,
                      ),
                      child: Padding(
                        padding: UIPadding.padding_8,
                        child: Text(
                          'Cancelar',
                          style: bodyMediumRegular,
                        ),
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: model.selected == -1
                        ? null
                        : () => model.onSave(context),
                    child: Card(
                      color: model.selected == -1
                          ? UIColors.gray
                          : UIColors.mediumSeaGreen,
                      elevation: UILayout.xsmall,
                      shape: RoundedRectangleBorder(
                        borderRadius: radius4,
                      ),
                      child: Padding(
                        padding: UIPadding.padding_8,
                        child: Text(
                          'Enviar',
                          style: bodyMediumRegular,
                        ),
                      ),
                    ),
                  ),
                ],
              ));
              return resp;
            }()),
          ),
        ),
      ),
    );
  }
}
