import 'package:flutter/material.dart';
import 'package:main_app/ui/models/sort_dialog_view_model.dart';
import 'package:stacked/stacked.dart';
import 'package:ui_shared/ui_shared.dart';

class SortDialog extends StatelessWidget {
  const SortDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder.reactive(
      viewModelBuilder: () => SortDialogViewModel(),
      builder: (BuildContext context, SortDialogViewModel model, _) =>
          AlertDialog(
        title: Text(
          'Ordenar por:',
          style: h3,
        ),
        backgroundColor: UIColors.eggshell,
        content: ConstrainedBox(
          constraints: const BoxConstraints(maxHeight: 200),
          child: Column(
            children: (() {
              List<Widget> resp = [];
              int selected = model.getCurrentAction();
              String act;
              for (int i = 0; i < model.getActions().length; i++) {
                act = model.getActions()[i];
                bool isSelected = i == selected;
                resp.add(
                  TextButton(
                    onPressed: isSelected
                        ? () {}
                        : () {
                            model.selectAction(i);
                            Navigator.of(context).pop();
                          },
                    child: Card(
                      color:
                          isSelected ? UIColors.middleRed : UIColors.eggshell,
                      elevation: UILayout.xsmall,
                      shape: RoundedRectangleBorder(
                        borderRadius: radius4,
                      ),
                      child: Padding(
                        padding: UIPadding.padding_8,
                        child: Text(
                          act,
                          style: bodyMediumRegular,
                        ),
                      ),
                    ),
                  ),
                );
              }
              return resp;
            }()),
          ),
        ),
      ),
    );
  }
}
