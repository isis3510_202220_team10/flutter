import 'package:flutter/material.dart';
import 'package:main_app/models/user_model.dart';
import 'package:main_app/services/user_service.dart';
import 'package:main_app/ui/models/base_view.dart';

class TechnologicalExpertiseDialogViewModel extends BaseViewModel {
  static const options = ['Baja', 'Media', 'Alta'];
  final UserService userService = UserService();
  int _selected = -1;

  int get selected => _selected;

  void selectTechnicalSkill(int selection) {
    _selected = selection == _selected ? -1 : selection;
    notifyListeners();
  }

  void onSave(BuildContext context) {
    userService.answeredTechnicalQuestion(options[_selected]);
    Navigator.of(context).pop();
  }

  void onCancel(BuildContext context) {
    UserModel user = userService.getUser()!;
    user.lastAsked = DateTime.now();
    userService.updateUser(user);
    userService.updateUserFirebase(user);
    Navigator.of(context).pop();
  }
}
