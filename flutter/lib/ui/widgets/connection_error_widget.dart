import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:ui_shared/ui_shared.dart';

class ConnectionErrorWidget extends StatelessWidget {
  final String? errmsg;
  final Function onRetry;
  const ConnectionErrorWidget({Key? key, this.errmsg, required this.onRetry})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Connectivity().checkConnectivity(),
      builder: (context, snapshot) =>
          snapshot.connectionState == ConnectionState.waiting
              ? const Center(
                  child: CircularProgressIndicator(
                    color: UIColors.eggshell,
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Column(
                        children: [
                          Text(
                            snapshot.data == ConnectivityResult.none
                                ? 'No hay conexión a internet'
                                : errmsg!,
                            textAlign: TextAlign.center,
                            style: h0.copyWith(
                              color: UIColors.blackCoffee,
                            ),
                          ),
                          if (snapshot.data == ConnectivityResult.none)
                            TextButton(
                              onPressed: () => onRetry(),
                              child: Card(
                                color: UIColors.middleRed,
                                elevation: UILayout.xsmall,
                                shape: RoundedRectangleBorder(
                                  borderRadius: radius4,
                                ),
                                child: Padding(
                                  padding: UIPadding.padding_8,
                                  child: Text(
                                    'Reintentar',
                                    style: bodyLargeBold,
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
    );
  }
}
