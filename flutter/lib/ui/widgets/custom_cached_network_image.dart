import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:main_app/helpers/error_helper.dart';
import 'package:ui_shared/ui_shared.dart';

class CustomCachedNetworkImage extends StatelessWidget {
  final List<String>? images;
  const CustomCachedNetworkImage({Key? key, this.images}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return images != null && images!.isNotEmpty
        ? CachedNetworkImage(
            imageUrl: images![0],
            height: UILayout.imageSize,
            width: UILayout.imageSize,
            fit: BoxFit.cover,
            placeholder: (context, url) => const CircularProgressIndicator(
              color: UIColors.eggshell,
            ),
            errorWidget: (context, url, error) => Container(
              padding: UIPadding.paddingV24,
              color: UIColors.middleRed,
              child: FutureBuilder(
                future: Connectivity().checkConnectivity(),
                builder: (context, snapshot) =>
                    snapshot.connectionState == ConnectionState.waiting
                        ? const CircularProgressIndicator(
                            color: UIColors.eggshell,
                          )
                        : Text(
                            snapshot.data == ConnectivityResult.none
                                ? 'No hay conexión a internet'
                                : ErrorHelper.getErrorMessage(
                                    error,
                                  ),
                            // getErrorMessage(error),
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                                backgroundColor: Colors.transparent),
                          ),
              ),
            ),
          )
        : const Icon(
            Icons.image,
            color: UIColors.blackCoffee,
            size: UILayout.imageSize,
          );
  }
}
