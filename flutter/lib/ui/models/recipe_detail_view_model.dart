part of ui.models.base;

//Top-level function so that it can be called in an isolate.
calculateIngredients(SendPort sendPort) async {
  final ReceivePort receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);
  receivePort.listen((message) {
    int diners = message[0];
    List<IngredientModel> ingredients = message[1] as List<IngredientModel>;
    final stopwatch = Stopwatch()..start();
    IngredientModel act;
    for (int i = 0; i < ingredients.length; i++) {
      act = ingredients[i];
      act.quantity =
          act.quantity != null ? act.quantity! * diners : act.quantity;
    }
    sendPort.send(['ingredients', ingredients]);
    int elapsedTime = stopwatch.elapsed.inMilliseconds;
    Map<String, dynamic>? info;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = message[2];
      info = {
        'phone': '${androidInfo.manufacturer} ${androidInfo.model}',
        'os': 'Android',
        'os_version': androidInfo.version.release,
        'times': [elapsedTime],
      };
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = message[2];
      info = {
        'phone': iosInfo.name,
        'os': iosInfo.systemName,
        'os_version': iosInfo.systemVersion,
        'times': [elapsedTime],
      };
    }
    sendPort.send(['info', info]);
  });
}

class RecipeDetailViewModel extends BaseViewModel {
  late List<IngredientModel> _ingredients;
  int diners = 1;
  bool loading = false;
  bool canEdit = false;
  final UserService userService = UserService();

  void initState(RecipeModel recipe) => _ingredients = recipe.ingredients;

  void toggleFavorite(RecipeModel recipe, BuildContext context) {
    recipe.toggleFavorite();
    Connectivity().checkConnectivity().then(
      (value) {
        if (value == ConnectivityResult.none) {
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              backgroundColor: UIColors.middleRed,
              content: Text(
                'No hay conexión, pero se guardará el cambio localmente mientras se recupera.',
                style: bodyMediumRegular.copyWith(
                  color: UIColors.blackCoffee,
                ),
              ),
            ),
          );
        }
      },
    );
    notifyListeners();
  }

  Future<String> get userIdActive async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Map<String, dynamic> response = json.decode(
      preferences.getString('userData') ?? '',
    ) as Map<String, dynamic>;
    return response['userId'];
  }

  Future<void> canEditRecipe(String userId) async {
    canEdit = (userId == (await userIdActive));
    notifyListeners();
  }

  void increaseDiners(List<IngredientModel> ingredients) {
    toggleLoading();
    diners++;
    notifyListeners();
    updateIngredients(ingredients);
  }

  void decreaseDiners(List<IngredientModel> ingredients) {
    toggleLoading();
    diners--;
    notifyListeners();
    updateIngredients(ingredients);
  }

  void toggleLoading() => loading = !loading;

  String getDuration(RecipeModel recipe) => recipe.duration >= 960
      ? '16h+'
      : recipe.duration >= 60
          ? '${(recipe.duration / 60).floor()}H:${recipe.duration % 60}m '
          : '${recipe.duration}m';

  Future<void> updateIngredients(List<IngredientModel> ingredients) async {
    ReceivePort? receivePort = ReceivePort();
    StreamQueue? recieveQueue = StreamQueue(receivePort);
    Isolate isolate =
        await Isolate.spawn(calculateIngredients, receivePort.sendPort);
    var sendport = await recieveQueue.next;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    var device = Platform.isAndroid
        ? await deviceInfo.androidInfo
        : await deviceInfo.iosInfo;
    sendport.send([diners, ingredients, device]);
    recieveQueue.rest.listen((message) {
      if (message[0] == 'ingredients') {
        _ingredients = message[1] as List<IngredientModel>;
        toggleLoading();
        notifyListeners();
      } else if (message[0] == 'info') {
        DocumentReference doc = FirebaseFirestore.instance
            .collection("t5n2")
            .doc(message[1]['phone']);
        doc.get().then((DocumentSnapshot docSnapshot) {
          if (docSnapshot.exists) {
            Map<String, dynamic> info =
                docSnapshot.data() as Map<String, dynamic>;
            info['times'] = [...info['times'], ...message[1]['times']];
            doc.update(info);
          } else {
            FirebaseFirestore.instance
                .collection("t5n2")
                .doc(message[1]['phone'])
                .set(message[1]);
          }
          isolate.kill();
        });
      }
    });
  }

  void onAdd(BuildContext context) {
    userService.addToShoppingList([..._ingredients]);
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: UIColors.mediumSeaGreen,
        duration: const Duration(
          milliseconds: 1500,
        ),
        content: Text(
          'Ingredientes agregados a la lista de compras',
          style: bodyMediumRegular.copyWith(
            color: UIColors.blackCoffee,
          ),
        ),
      ),
    );
  }

  List<IngredientModel> get ingredients => _ingredients;
}
