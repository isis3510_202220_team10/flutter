import 'package:flutter/material.dart';
import 'package:main_app/models/ingredient_model.dart';
import 'package:main_app/services/ingredients_service.dart';
import 'package:main_app/services/user_service.dart';
import 'package:main_app/ui/models/base_view.dart';
import 'package:ui_shared/ui_shared.dart';

class ShoppingListItemViewModel extends BaseViewModel {
  late IngredientModel _ingredient;
  double? _quantity;
  TextEditingController textController = TextEditingController();
  IngredientsService ingredientsService = IngredientsService();
  UserService userService = UserService();

  void initState(IngredientModel ingredient) {
    _ingredient = ingredient;
    _quantity = ingredient.quantity;
    textController..text = '${ingredient.quantity}';
    textController
        .addListener(() => _quantity = double.parse(textController.text));
  }

  void editShoppingListIngredient(BuildContext context) {
    _ingredient.quantity = _quantity;
    userService.editShoppingListIngredient(_ingredient);
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: UIColors.mediumSeaGreen,
        duration: const Duration(
          seconds: 1,
        ),
        content: Text(
          'Cambio guardado exitosamente',
          style: bodyMediumRegular.copyWith(
            color: UIColors.blackCoffee,
          ),
        ),
      ),
    );
  }

  void deleteIngredientFromShoppingList() =>
      userService.deleteItemFromShoppingList(_ingredient.reference);
}
