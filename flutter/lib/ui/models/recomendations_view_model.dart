part of ui.models.base;

class RecomendationsViewModel extends BaseViewModel {
  static final UserService userService = UserService();
  static final RecipesService recipesService = RecipesService();
  List<RecipeModel> recomendations = [];
  late StreamSubscription<List<RecipeModel>> recomendationsStream;
  Future<void> initState() async {
    recomendationsStream = userService.recomendations.listen((event) async {
      recomendations = await recipesService.getRecomendedRecipes();
    });
    recomendations = await recipesService.getRecomendedRecipes();
    notifyListeners();
  }

  @override
  void dispose() {
    try {
      recomendationsStream.cancel();
      super.dispose();
    } catch (e) {}
  }
}
