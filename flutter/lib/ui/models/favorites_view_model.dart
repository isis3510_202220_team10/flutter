part of ui.models.base;

class FavoritesViewModel extends BaseViewModel {
  static final UserService userService = UserService();
  List<RecipeModel> favorites = [];
  late StreamSubscription<List<RecipeModel>> favoritesStream;
  void initState() {
    favorites = userService.getUser()!.favorites;
    favoritesStream =
        userService.favorites.listen((List<RecipeModel> pFavorites) {
      favorites = pFavorites;
      notifyListeners();
    });
  }

  @override
  void dispose() {
    try {
      favoritesStream.cancel();
      super.dispose();
    } catch (e) {}
  }
}
