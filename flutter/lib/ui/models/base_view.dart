library ui.models.base;

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';

import 'package:async/async.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:main_app/helpers/error_helper.dart';
import 'package:main_app/models/ingredient_model.dart';
import 'package:main_app/services/ingredients_service.dart';
import 'package:main_app/services/recipes_service.dart';
import 'package:main_app/services/statistics_service.dart';
import 'package:main_app/services/user_service.dart';
import 'package:main_app/ui/views/screens.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked/stacked.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:ui_shared/ui_shared.dart';

import '../../models/bar_chart_data_model.dart';
import '../../models/recipe_model.dart';

part './add_recipe_view.dart';
part './recipes_list_view_model.dart';
part './statistics_view_model.dart';
part './favorites_view_model.dart';
part './recomendations_view_model.dart';
part './recipe_detail_view_model.dart';
part './shopping_list_view_model.dart';

class BaseViewModel extends ReactiveViewModel {

  @override
  List<ReactiveServiceMixin> get reactiveServices => <ReactiveServiceMixin>[];
}
