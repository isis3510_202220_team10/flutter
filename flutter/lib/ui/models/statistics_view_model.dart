part of ui.models.base;

class StatisticsViewModel extends BaseViewModel {
  final StatisticsService statisticsService = StatisticsService();
  static final UserService userService = UserService();
  final ScrollController scrollController = ScrollController();

  Map<String, dynamic> finantialData = <String, dynamic>{};
  List<BarCharDataModel> caloricData = <BarCharDataModel>[];
  List<Color> pieChartColors = <Color>[
    UIColors.blackCoffee,
    UIColors.mediumSeaGreen,
    UIColors.middleRed,
    Color.fromARGB(255, 157, 96, 198),
    Color.fromARGB(255, 201, 179, 57),
    Color.fromARGB(255, 228, 131, 45),
  ];
  Future<void> getUserCaloricIntake() async {
    try {
      caloricData =
          await statisticsService.getUserCaloricIntake(userService.userId);
    } catch (e) {
      rethrow;
    }
  }

  int getTotalCaloricIntake(List<BarCharDataModel> data) {
    int sum = 0;
    for (int i = 0; i < data.length; i++) {
      sum += data[i].measure;
    }
    return sum;
  }

  List<Map<String, dynamic>> getJsonList(List<BarCharDataModel> data) {
    List<Map<String, dynamic>> resp = [];
    for (int i = 0; i < data.length; i++) {
      resp.add(data[i].toJson());
    }
    return resp;
  }

  Future<void> getUserFinantialWaste() async {
    try {
      Map<String, dynamic> resp =
          await statisticsService.getUserFinantialWaste(userService.userId);
      resp['13'] = getTotalFinantialWaste(resp);
      Map<String, dynamic> sortedMap = Map.fromEntries(resp.entries.toList()
        ..sort((e1, e2) => (int.parse(e1.key)).compareTo((int.parse(e2.key)))));
      finantialData = sortedMap;
    } catch (e) {
      rethrow;
    }
  }

  int getTotalFinantialWaste(Map<String, dynamic> data) {
    int sum = 0;
    for (int i = 0; i < data.length; i++) {
      sum += data.values.elementAt(i) as int;
    }
    return sum;
  }

  Future<void> requestData() async {
    setBusy(true);
    try {
      await getUserCaloricIntake();
      await getUserFinantialWaste();
    } catch (e) {
      setError(e);
    }

    setBusy(false);
  }
}
