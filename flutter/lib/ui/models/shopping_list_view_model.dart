part of ui.models.base;

class ShoppingListViewModel extends BaseViewModel {
  IngredientsService ingredientsService = IngredientsService();
  final UserService userService = UserService();
  List<IngredientModel> shoppingList = [];
  late StreamSubscription<List<IngredientModel>> shoppingListStream;

  void initState() {
    shoppingList = userService.getUser()!.shoppingList;
    shoppingListStream =
        userService.shoppingList.listen((List<IngredientModel> pShoppingList) {
      shoppingList = pShoppingList;
      notifyListeners();
    });
  }

  @override
  void dispose() {
    try {
      shoppingListStream.cancel();
      super.dispose();
    } catch (e) {}
  }
}
