part of ui.models.base;

class AddRecipeViewModel extends BaseViewModel {
  bool haveConnection = true;

  ScrollController scrollController = ScrollController();
  Connectivity connectivity = Connectivity();

  String recipeId = '';
  bool isEditing = false;

  Map<String, Map<String, dynamic>> ingredientsQuantity =
      <String, Map<String, dynamic>>{};

  Map<String, Map<String, dynamic>> controllers =
      <String, Map<String, dynamic>>{
    'name': <String, dynamic>{
      'controller': TextEditingController(),
      'error': '',
      'isMandatory': true,
    },
    'description': <String, dynamic>{
      'controller': TextEditingController(),
      'error': '',
      'isMandatory': false,
    },
    'steps': <String, dynamic>{
      'controller': TextEditingController(),
      'error': '',
      'isMandatory': true,
    },
    'time': <String, dynamic>{
      'controller': TextEditingController(),
      'error': '',
      'isMandatory': true,
    },
    'ingredients': <String, dynamic>{
      'controller': TextEditingController(),
      'focus': FocusNode(),
      'error': '',
      'isMandatory': true,
    },
    'type': <String, dynamic>{
      'controller': TextEditingController(),
      'focus': FocusNode(),
      'error': '',
      'isMandatory': false,
    },
    'images': <String, dynamic>{
      'error': '',
      'isMandatory': true,
    }
  };

  String getErrorController(String key) => controllers[key]?['error'] as String;

  ImagePicker picker = ImagePicker();
  List<File> images = <File>[];
  List<String> fireImages = <String>[];
  ConnectivityResult result = ConnectivityResult.none;

  // List<String> typesOfFood = <String>[
  //   'a',
  //   'b',
  //   'c',
  //   'd',
  //   'e',
  //   'f',
  //   'g',
  //   'b',
  //   'c'
  // ];
  // List<String> selectedTypesOfFood = <String>[];

  List<Map<String, dynamic>> ingredients = <Map<String, dynamic>>[];
  List<String> selectedIngredients = <String>[];

  List<String> measumentsUnits = <String>[
    '',
    'ml de',
    'l de',
    'mg de',
    'g de',
    'kg de',
    'tsp de',
    'tbsp de'
  ];

  FirebaseFirestore db = FirebaseFirestore.instance;

  int get imagesLength => images.length;

  void getParams(RecipeModel recipe) {
    TextEditingController controller;
    for (int i = 0; i < controllers.length - 1; i++) {
      controller = controllers.values.elementAt(i)['controller']
          as TextEditingController;
      if (controllers.keys.elementAt(i) == 'name') {
        controller.text = recipe.name;
      } else if (controllers.keys.elementAt(i) == 'description') {
        controller.text = recipe.description;
      } else if (controllers.keys.elementAt(i) == 'time') {
        controller.text = '${recipe.duration}';
      } else if (controllers.keys.elementAt(i) == 'steps') {
        controller.text = recipe.process;
      } else if (controllers.keys.elementAt(i) == 'type') {
        controller.text = recipe.type ?? '';
      } else if (controllers.keys.elementAt(i) == 'ingredients') {
        List<IngredientModel> recipeIngredients = recipe.ingredients;
        IngredientModel ingredient;
        for (int i = 0; i < recipeIngredients.length; i++) {
          ingredient = recipeIngredients[i];
          Map<String, dynamic> ingredientMap = ingredients.firstWhere(
            (Map<String, dynamic> element) =>
                (element['name'] as String) == ingredient.name,
          );
          ingredientsQuantity[ingredient.name] = {
            'controller': TextEditingController(),
            'controller2': TextEditingController(),
            'focus': FocusNode(),
            'quantity': ingredient.quantity,
            'unit': ingredient.unit,
            ...ingredientMap,
          };
          (ingredientsQuantity[ingredient.name]!['controller']
                  as TextEditingController)
              .text = (ingredient.quantity ?? 0).toString();
          (ingredientsQuantity[ingredient.name]!['controller2']
                  as TextEditingController)
              .text = ingredient.unit ?? '';
          selectedIngredients.add(ingredient.name);
        }
      }
    }
    fireImages.addAll(recipe.images ?? <String>[]);
    notifyListeners();
  }

  List<String> get ingredientsNames => ingredients
      .map((Map<String, dynamic> elmt) => elmt['name'] as String? ?? '')
      .toList();

  TextEditingController getIngredientController(int index, bool isQuantity) {
    String value = selectedIngredients[index];
    return ingredientsQuantity[value]![
        isQuantity ? 'controller' : 'controller2'] as TextEditingController;
  }

  FocusNode getIngredientFocus(int index) {
    String value = selectedIngredients[index];
    return ingredientsQuantity[value]!['focus'] as FocusNode;
  }

  void initEditView(RecipeModel recipe) async {
    await getIngredientsFromDB();
    getParams(recipe);
  }

  Future<void> getIngredientsFromDB() async {
    final metric = FirebasePerformance.instance
        .newHttpMetric("https://www.google.com", HttpMethod.Get);

    await metric.start();
    await db.collection("ingredients").get().then((event) {
      for (QueryDocumentSnapshot<Map<String, dynamic>> doc in event.docs) {
        ingredients.add(doc.data());
      }
    });
    await metric.stop();
  }

  Future<void> getLostData() async {
    final LostDataResponse response = await picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.files != null) {
      for (final XFile file in response.files!) {
        images.add(File(file.path));
      }
    } else {
      debugPrint(response.exception.toString());
    }
  }

  Future<void> pickImageCamera() async {
    try {
      final XFile? image = await ImagePicker().pickImage(
        source: ImageSource.camera,
      );
      if (image == null) return;
      addImage(File(image.path));
      notifyListeners();
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image: $e');
    }
  }

  Future<void> pickImageGallery() async {
    try {
      List<XFile> imagesPicked = await ImagePicker().pickMultiImage();
      if (imagesPicked.isEmpty) return;
      for (XFile image in imagesPicked) {
        addImage(File(image.path));
      }
      notifyListeners();
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image: $e');
    }
  }

  void addImage(File file) {
    if (isEditing && ((images.length + fireImages.length) < 5)) {
      images.add(file);
    } else {
      if (images.length < 5) {
        images.add(file);
      }
    }
  }

  Future<List<String>> saveImages() async {
    final FirebaseStorage storage = FirebaseStorage.instance;
    List<String> urls = <String>[];
    for (File img in images) {
      TaskSnapshot taskSnapshot =
          await storage.ref('recipesImages/${img.path}').putFile(img);
      final String downloadUrl = await taskSnapshot.ref.getDownloadURL();
      urls.add(downloadUrl);
    }
    return urls;
  }

  Future<void> checkConnectivity(BuildContext context) async {
    result = await connectivity.checkConnectivity();
    if (result == ConnectivityResult.none) {
      showModal(
        context,
        ModalNoNetworkToAdd(
          retryCheck: checkConnectivity,
          loadAnyway: loadAnyway,
        ),
      );
    } else if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      if (isEditing) {
        updateData(context);
      } else {
        loadData(context);
      }
    }
  }

  void loadAnyway(BuildContext context) {
    showModal(
      context,
      ModalLoadAnyway(
        loadFcn: isEditing ? updateData : loadData,
      ),
    );
  }

  Future<void> loadData(BuildContext context) async {
    bool isValid = validateForm();
    if (isValid) {
      showModal(
        context,
        const ModalSuccessAdd(),
      );

      result = await connectivity.checkConnectivity();

      List<String> urls = [];
      if (images.isNotEmpty && result != ConnectivityResult.none) {
        urls = await saveImages();
      }

      SharedPreferences preferences = await SharedPreferences.getInstance();
      Map<String, dynamic> response = json.decode(
        preferences.getString('userData') ?? '',
      ) as Map<String, dynamic>;

      Map<String, dynamic> recipe = <String, dynamic>{
        'userId': response['userId'],
        'name': getTextController('name'),
        'description': getTextController('description'),
        'process': getTextController('steps'),
        'duration': getTextController('time'),
        'type': getTextController('type'),
        'images': urls,
        'ingredients': getIngredients(),
      };

      cleanFields();
      db.collection("recipes").add(recipe);
    }
  }

  Future<void> updateData(BuildContext context) async {
    final recipeRef = db.collection("recipes").doc(recipeId);

    bool isValid = validateForm();
    if (isValid) {
      showModal(
        context,
        ModalSuccessAdd(
          isEditing: isEditing,
        ),
      );

      result = await connectivity.checkConnectivity();

      List<String> urls = [...fireImages];
      if (images.isNotEmpty && result != ConnectivityResult.none) {
        urls = await saveImages();
      }

      SharedPreferences preferences = await SharedPreferences.getInstance();
      Map<String, dynamic> response = json.decode(
        preferences.getString('userData') ?? '',
      ) as Map<String, dynamic>;

      Map<String, dynamic> recipe = <String, dynamic>{
        'userId': response['userId'],
        'name': getTextController('name'),
        'description': getTextController('description'),
        'process': getTextController('steps'),
        'duration': getTextController('time'),
        'type': getTextController('type'),
        'images': urls,
        'ingredients': getIngredients(),
      };

      cleanFields();
      recipeRef.update(recipe);
    }
  }

  List<Map<String, dynamic>> getIngredients() {
    return ingredientsQuantity.values.toList().map((Map<String, dynamic> elmt) {
      elmt.remove('controller');
      elmt.remove('controller2');
      elmt.remove('focus');
      return elmt;
    }).toList();
  }

  String getTextController(String key) {
    return (controllers[key]!['controller'] as TextEditingController).text;
  }

  void removeImage(int index) {
    images.remove(images.elementAt(index));
    notifyListeners();
  }

  void removeFireImage(int index) {
    fireImages.remove(fireImages.elementAt(index));
    notifyListeners();
  }

  void showModal(
    BuildContext context,
    Widget content, {
    bool dismissible = true,
  }) {
    showDialog(
      barrierDismissible: dismissible,
      context: context,
      builder: (BuildContext context) => content,
    );
  }

  void requestedField(String value, String controllerName) {
    if (value.isEmpty) {
      controllers[controllerName]!['error'] = 'Este campo es obligatorio';
    } else {
      controllers[controllerName]!['error'] = '';
    }
    notifyListeners();
  }

  void formatTime(String name) {
    TextEditingController controller =
        controllers['time']!['controller'] as TextEditingController;
    controller.text = int.parse(controller.text).toString();
  }

  // void onChangeTypesFood(String value) {
  //   selectedTypesOfFood.add(value);
  //   (controllers['type']!['controller'] as TextEditingController).text = '';
  //   (controllers['type']!['focus'] as FocusNode).unfocus();
  //   notifyListeners();
  // }

  void onChangeIngredients(String value) {
    selectedIngredients.add(value);
    (controllers['ingredients']!['controller'] as TextEditingController).text =
        '';
    (controllers['ingredients']!['focus'] as FocusNode).unfocus();
    Map<String, dynamic> ingredient = ingredients.firstWhere(
      (Map<String, dynamic> element) => element['name'] == value,
    );
    ingredientsQuantity[value] = <String, dynamic>{
      'controller': TextEditingController(),
      'controller2': TextEditingController(),
      'focus': FocusNode(),
      'quantity': 0,
      'unit': '',
      ...ingredient,
    };
    notifyListeners();
  }

  // void eraseSelectedType(int index) {
  //   selectedTypesOfFood.removeAt(index);
  //   notifyListeners();
  // }

  void eraseSelectedIngredient(int index) {
    String value = selectedIngredients.removeAt(index);
    ingredientsQuantity.remove(value);
    notifyListeners();
  }

  void onChangedQuantity(String value, int index) {
    String ingredient = selectedIngredients[index];
    ingredientsQuantity[ingredient]!['quantity'] = value;
    notifyListeners();
  }

  void onChangedUnit(String value, int index) {
    String ingredient = selectedIngredients[index];
    ingredientsQuantity[ingredient]!['unit'] = value;
    (ingredientsQuantity[ingredient]!['focus'] as FocusNode).unfocus();
    notifyListeners();
  }

  bool validateForm() {
    bool result = true;
    for (int i = 0; i < controllers.keys.length; i++) {
      String element = controllers.keys.elementAt(i);
      Map<String, dynamic> value = controllers[element]!;
      if (value['isMandatory'] as bool) {
        if (element == 'ingredients') {
          if (selectedIngredients.isEmpty) {
            value['error'] = 'Debe ingresar al menos un ingrediente';
          }
        } else if (element == 'images') {
          if (images.isEmpty && (!isEditing && fireImages.isEmpty)) {
            value['error'] = 'Debe seleccionar al menos una imagen';
          }
        } else {
          if ((value['controller'] as TextEditingController).text.isEmpty) {
            value['error'] = 'Este campo es obligatorio';
          }
        }
      }
    }
    notifyListeners();
    for (int i = 0; i < controllers.keys.length && result == true; i++) {
      String element = controllers.keys.elementAt(i);
      Map<String, dynamic> value = controllers[element]!;
      if ((value['error'] as String).isNotEmpty) {
        result = false;
      }
    }
    return result;
  }

  void cleanFields() {
    for (int i = 0; i < controllers.length; i++) {
      Map<String, dynamic> value = controllers.entries.elementAt(i).value;
      if (value['controller'] != null) {
        (value['controller'] as TextEditingController).clear();
      }
      value['error'] = '';
    }
    selectedIngredients.clear();
    images.clear();
    fireImages.clear();
    notifyListeners();
  }
}
