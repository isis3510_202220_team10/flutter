part of ui.models.base;

class RecipesListViewModel extends BaseViewModel {
  bool loading = true;
  String? errorMessage;
  List<RecipeModel> recipes = [];
  List<RecipeModel> recipesDisplay = [];
  final RecipesService recipesService = RecipesService();
  ScrollController scrollController = ScrollController();
  TextEditingController textEditingController = TextEditingController();
  late final double treshold;
  late double maxScroll;
  late double actScroll;

  void initState(BuildContext context) {
    treshold = MediaQuery.of(context).size.height * 0.25;
    textEditingController
        .addListener(() => onSearch(textEditingController.text.toLowerCase()));
    recipesService.getRecipes().then(setRecipes).catchError(onError);
    scrollController.addListener(() {
      maxScroll = scrollController.position.maxScrollExtent;
      actScroll = scrollController.position.pixels;
      if (maxScroll - actScroll <= treshold) {
        loading = true;
        notifyListeners();
        recipesService.getMoreRecipes().then(setRecipes).catchError(onError);
      }
    });
  }

  void onSearch(String input) {
    if (input == '') {
      recipesDisplay = recipes;
      notifyListeners();
      return;
    }
    RecipeModel act;
    recipesDisplay = [];
    for (int i = 0; i < recipes.length; i++) {
      act = recipes[i];
      if (act.name.toLowerCase().contains(input)) {
        recipesDisplay.add(act);
      }
    }
    notifyListeners();
  }

  void onRetry() {
    recipesDisplay = [];
    loading = true;
    notifyListeners();
    recipesService.getRecipes().then(setRecipes).catchError(onError);
  }

  void setRecipes(List<RecipeModel> pRecipes) {
    errorMessage = null;
    loading = false;
    recipes = pRecipes;
    recipesDisplay = recipes;
    notifyListeners();
  }

  void onError(Object err) {
    loading = false;
    errorMessage = ErrorHelper.getErrorMessage(err);
    notifyListeners();
  }
}
