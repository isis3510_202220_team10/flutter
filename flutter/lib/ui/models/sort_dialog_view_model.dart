import 'package:main_app/services/recipes_service.dart';
import 'package:main_app/ui/models/base_view.dart';

class SortDialogViewModel extends BaseViewModel {
  final RecipesService _recipesService = RecipesService();
  List<String> getActions() => _recipesService.sortByOptionsSpanish;
  int getCurrentAction() => _recipesService.sortBy;
  void selectAction(int attribute) {
    _recipesService.sortQueryBy(attribute);
    notifyListeners();
  }
}
