import 'dart:async';
import 'dart:convert';

import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:main_app/helpers/hive_boxes_helper.dart';
import 'package:main_app/models/http_exception.dart';
import 'package:main_app/models/user_model.dart';
import 'package:main_app/services/user_service.dart';
import 'package:main_app/ui/views/screens.dart';
import 'package:main_app/ui/widgets/technological_expertise_dialog.dart';
import 'package:main_app/ui/widgets/technological_expertise_dialog_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Auth with ChangeNotifier {
  String? _token = null;
  DateTime? _expiryDate = null;
  String? _userId = null;
  Timer? _authTimer = null;
  UserService userService = UserService();

  bool get isAuth {
    return token != null;
  }

  String? get token {
    if (_expiryDate != null &&
        _expiryDate!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token!;
    }
    return null;
  }

  String? get userId {
    return _userId;
  }

  Future<void> _authenticate(
      String email, String password, String urlSegment, String _name) async {
    final url = Uri.parse(
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/$urlSegment?key=AIzaSyBowDEYRFgkbN1XvQo8EqWh5C7HTlMYY94');
    try {
      final metric = FirebasePerformance.instance
          .newHttpMetric("https://www.google.com", HttpMethod.Post);

      await metric.start();
      final response = await http.post(
        url,
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );
      final responseData = json.decode(response.body);
      await metric.stop();
      if (responseData['error'] != null) {
        throw HttpException(responseData['error']['message']);
      }
      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _expiryDate = DateTime.now().add(
        Duration(
          seconds: int.parse(
            responseData['expiresIn'],
          ),
        ),
      );
      userService.userId = _userId ?? '';
      UserModel? user = userService.getUser();
      if (user == null) {
        user = UserModel(
          id: _userId as String,
          name: _name,
          email: email,
          token: _token as String,
        );
        FirebaseFirestore.instance
            .collection("users")
            .doc(_userId)
            .get()
            .then((docSnapshot) {
          if (docSnapshot.exists &&
              docSnapshot.data()?['technicalSkill'] != null) {
            user!.technicalSkill = docSnapshot.data()!['technicalSkill'];
            HiveBoxesHelper.putUser(user);
          }
        });
      } else {
        user.token = _token!;
        user.name = _name;
      }
      HiveBoxesHelper.putUser(user);
      _autoLogout();
      notifyListeners();
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
        {
          'token': _token,
          'userId': _userId,
          'name': _name,
          'email': email,
          'expiryDate': _expiryDate!.toIso8601String(),
        },
      );
      prefs.setString('userData', userData);
      if (urlSegment == "signupNewUser") {
        uploadingData(user);
      }
    } catch (error) {
      rethrow;
    }
  }

  Future<void> signup(String email, String password, String name) async {
    return _authenticate(email, password, 'signupNewUser', name);
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'verifyPassword', "");
  }

  Future<void> uploadingData(UserModel user) async {
    await FirebaseFirestore.instance.collection("users").add(
      {
        'userId': user.id,
        'name': user.name,
        'email': user.email,
        'token': user.token,
        'lastAsked': user.lastAsked,
        'technicalSkill': user.technicalSkill
      },
    );
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final extractedUserData = json.decode(
      prefs.getString('userData') ?? '',
    ) as Map<String, dynamic>;
    final expiryDate = DateTime.parse(
      extractedUserData['expiryDate'],
    );

    if (expiryDate.isBefore(
      DateTime.now(),
    )) {
      return false;
    }
    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _expiryDate = expiryDate;
    userService.userId = userId ?? '';
    notifyListeners();
    UserModel? user = userService.getUser();
    if (user == null) {
      user = UserModel(
        id: _userId as String,
        name: extractedUserData['name'],
        email: extractedUserData['email'],
        token: _token as String,
      );
      FirebaseFirestore.instance
          .collection("users")
          .doc(_userId)
          .get()
          .then((docSnapshot) {
        if (docSnapshot.exists &&
            docSnapshot.data()?['technicalSkill'] != null) {
          user!.technicalSkill = docSnapshot.data()!['technicalSkill'];
          HiveBoxesHelper.putUser(user);
        }
      });
    } else {
      user.token = _token!;
      user.name = extractedUserData['name'];
    }
    HiveBoxesHelper.putUser(user);
    _autoLogout();
    return true;
  }

  Future<void> logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer!.cancel();
      _authTimer = null;
    }
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('userData');
    prefs.clear();

    notifyListeners();
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer!.cancel();
    }
    final timeToExpiry = _expiryDate!
        .difference(
          DateTime.now(),
        )
        .inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
