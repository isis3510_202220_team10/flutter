import 'package:hive/hive.dart';
part 'ingredient_model.g.dart';

//Class which represents the ingredient object
@HiveType(typeId: 1)
class IngredientModel extends HiveObject {
  static const shoppingListHiveBoxName = 'shoppingList';
  //Attributes
  @HiveField(0)
  String name;
  @HiveField(2)
  String reference;
  @HiveField(3)
  double? quantity;
  @HiveField(4)
  String? unit;
  //TODO: Price
  //double price;

  //Constructor, returns IngredientModel object
  IngredientModel({
    required this.name,
    required this.reference,
    this.quantity,
    this.unit,
  });

  Map<String, dynamic> toJson() => {
        'name': name,
        'reference': reference,
        'quantity': quantity,
        'unit': unit
      };

  IngredientModel.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        reference = json['reference'],
        quantity = json['quantity'],
        unit = json['unit'];
}
