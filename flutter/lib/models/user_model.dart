import 'package:hive/hive.dart';
import 'package:main_app/helpers/hive_boxes_helper.dart';
import 'package:main_app/models/ingredient_model.dart';
import 'package:main_app/models/recipe_model.dart';

import '../ui/widgets/technological_expertise_dialog_view_model.dart';
part 'user_model.g.dart';

@HiveType(typeId: 2)
class UserModel extends HiveObject {
  static const hiveBoxName = 'users';
  //Attributes
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String email;
  @HiveField(3)
  String token;
  @HiveField(4, defaultValue: [])
  List<RecipeModel> favorites = [];
  @HiveField(5, defaultValue: [])
  List<RecipeModel> recomendations = [];
  @HiveField(6, defaultValue: [])
  List<IngredientModel> shoppingList = [];
  @HiveField(7, defaultValue: null)
  DateTime? lastAsked;
  @HiveField(8, defaultValue: null)
  String? technicalSkill;

  //Constructor, returns UserModel instance
  UserModel({
    required this.id,
    required this.name,
    required this.email,
    required this.token,
  });

  void addFavorite(RecipeModel recipe) {
    favorites.add(recipe);
    HiveBoxesHelper.putUser(this);
  }

  void addRecomendation(RecipeModel recipe) {
    recomendations.add(recipe);
    HiveBoxesHelper.putUser(this);
  }

  void removeFavorite(RecipeModel recipe) {
    int indexToRemove =
        favorites.indexWhere((element) => element.id == recipe.id);
    favorites.removeAt(indexToRemove);
    HiveBoxesHelper.putUser(this);
  }

  void removeRecomendation(RecipeModel recipe) {
    int indexToRemove =
        recomendations.indexWhere((element) => element.id == recipe.id);
    recomendations.removeAt(indexToRemove);
    HiveBoxesHelper.putUser(this);
  }

  void toggleFavorite(RecipeModel recipe) =>
      recipe.favorite ? addFavorite(recipe) : removeFavorite(recipe);

  void toggleRecomendation(RecipeModel recipe) => recipe.recomended
      ? addRecomendation(recipe)
      : removeRecomendation(recipe);

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'token': token,
        'lastAsked': lastAsked,
        'technicalSkill': technicalSkill
      };

  UserModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        email = json['email'],
        token = json['token'],
        lastAsked = json['lastAsked'],
        technicalSkill = json['technicalSkill'];
}
