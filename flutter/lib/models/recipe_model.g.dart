// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipe_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class RecipeModelAdapter extends TypeAdapter<RecipeModel> {
  @override
  final int typeId = 0;

  @override
  RecipeModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return RecipeModel(
      name: fields[1] as String,
      description: fields[2] as String,
      duration: fields[3] as int,
      rate: fields[4] as double,
      ingredients: (fields[5] as List).cast<IngredientModel>(),
      process: fields[6] as String,
      calories: fields[7] as int,
      images: (fields[8] as List?)?.cast<String>(),
      id: fields[0] as String,
      type: fields[11] as String?,
      userId: fields[12] as String?,
      video: fields[9] as String?,
    )
      ..favorite = fields[10] == null ? false : fields[10] as bool
      ..recomended = fields[15] == null ? false : fields[15] as bool;
  }

  @override
  void write(BinaryWriter writer, RecipeModel obj) {
    writer
      ..writeByte(14)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.duration)
      ..writeByte(4)
      ..write(obj.rate)
      ..writeByte(5)
      ..write(obj.ingredients)
      ..writeByte(6)
      ..write(obj.process)
      ..writeByte(7)
      ..write(obj.calories)
      ..writeByte(8)
      ..write(obj.images)
      ..writeByte(9)
      ..write(obj.video)
      ..writeByte(10)
      ..write(obj.favorite)
      ..writeByte(15)
      ..write(obj.recomended)
      ..writeByte(11)
      ..write(obj.type)
      ..writeByte(12)
      ..write(obj.userId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RecipeModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
