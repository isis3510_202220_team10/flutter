//Class which represents the recipe object
import 'package:main_app/models/ingredient_model.dart';
import 'package:hive/hive.dart';

import '../helpers/hive_boxes_helper.dart';
import '../services/user_service.dart';
part 'recipe_model.g.dart';

@HiveType(typeId: 0)
class RecipeModel extends HiveObject {
  //Hive box name
  static const hiveBoxName = 'recipes';
  static final userService = UserService();
  //Attributes
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String description;
  @HiveField(3)
  int duration;
  @HiveField(4)
  double rate;
  @HiveField(5)
  List<IngredientModel> ingredients;
  @HiveField(6)
  String process;
  @HiveField(7)
  int calories;
  @HiveField(8)
  List<String>? images;
  @HiveField(9)
  String? video;
  @HiveField(10, defaultValue: false)
  bool favorite = false;
  @HiveField(15, defaultValue: false)
  bool recomended = false;
  @HiveField(11)
  String? type;
  @HiveField(12)
  String? userId;

  //Constructor, returns RecipeModel instance
  RecipeModel({
    required this.name,
    required this.description,
    required this.duration,
    required this.rate,
    required this.ingredients,
    required this.process,
    required this.calories,
    required this.images,
    required this.id,
    this.type,
    this.userId,
    this.video,
  });
  Map<String, dynamic> toJson() => {
        'name': name,
        'description': description,
        'duration': duration,
        'rate': rate,
        'ingredients': ingredients,
        'process': process,
        'calories': calories,
        'id': id,
        'images': images,
        'video': video,
        'favorite': favorite,
        'recomended': recomended,
        'type': type,
        'userId': userId,
      };

  RecipeModel.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        description = json['description'],
        duration = json['duration'] is int
            ? json['duration']
            : int.parse(json['duration']),
        rate = double.parse((json['rate'] ?? 0.0).toString()),
        ingredients = ingredientsFromJson(json['ingredients']),
        process = json['process'],
        images = imagesFromJson(json['images']),
        calories = json['calories'] ?? 0,
        id = json['id'],
        video = json['video'],
        type = json['type'] ?? '',
        userId = json['userId'] ?? '';

  static List<String>? imagesFromJson(List<dynamic> list) {
    List<String> resp = [];
    for (int i = 0; i < list.length; i++) {
      resp.add(list[i].toString());
    }
    return resp;
  }

  static List<IngredientModel> ingredientsFromJson(List<dynamic> list) {
    List<IngredientModel> response = [];
    dynamic act;
    for (int i = 0; i < list.length; i++) {
      act = list[i];
      response.add(IngredientModel(
        name: act['name'],
        reference: act['reference'],
        quantity: act['quantity'] != null
            ? double.parse(act['quantity'].toString())
            : null,
        unit: act['unit'],
      ));
    }
    return response;
  }

  void toggleFavorite() {
    favorite = !favorite;
    userService.toggleFavorite(this);
  }

  void toggleRecomendation() {
    recomended = !recomended;
    userService.toggleRecomendation(this);
  }
}
