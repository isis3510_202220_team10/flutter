// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserModelAdapter extends TypeAdapter<UserModel> {
  @override
  final int typeId = 2;

  @override
  UserModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserModel(
      id: fields[0] as String,
      name: fields[1] as String,
      email: fields[2] as String,
      token: fields[3] as String,
    )
      ..favorites =
          fields[4] == null ? [] : (fields[4] as List).cast<RecipeModel>()
      ..recomendations =
          fields[5] == null ? [] : (fields[5] as List).cast<RecipeModel>()
      ..shoppingList =
          fields[6] == null ? [] : (fields[6] as List).cast<IngredientModel>()
      ..lastAsked = fields[7] as DateTime?
      ..technicalSkill = fields[8] as String?;
  }

  @override
  void write(BinaryWriter writer, UserModel obj) {
    writer
      ..writeByte(9)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.email)
      ..writeByte(3)
      ..write(obj.token)
      ..writeByte(4)
      ..write(obj.favorites)
      ..writeByte(5)
      ..write(obj.recomendations)
      ..writeByte(6)
      ..write(obj.shoppingList)
      ..writeByte(7)
      ..write(obj.lastAsked)
      ..writeByte(8)
      ..write(obj.technicalSkill);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
