class BarCharDataModel {
  String domain;
  int measure;

  BarCharDataModel({
    required this.domain,
    required this.measure,
  });

  BarCharDataModel.fromJson(Map<String, dynamic> json)
      : domain = json['domain'],
        measure = json['measure'];

  Map<String, dynamic> toJson() => {
        'domain': domain,
        'measure': measure,
      };
}
