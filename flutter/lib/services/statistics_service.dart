import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:main_app/models/bar_chart_data_model.dart';

class StatisticsService {
  //ATTRIBUTES
  final CollectionReference caloricIntakeReference =
      FirebaseFirestore.instance.collection('caloricIntake');
  final CollectionReference finantialWasteReference =
      FirebaseFirestore.instance.collection('finantialWaste');

  //METHODS
  Future<List<BarCharDataModel>> getUserCaloricIntake(String uid) async {
    final metric = FirebasePerformance.instance
        .newHttpMetric("https://www.google.com", HttpMethod.Get);
    await metric.start();
    QuerySnapshot<Object?> dataRef = await caloricIntakeReference
        .where(
          'uid',
          isEqualTo: uid,
        )
        .get();
    await metric.stop();
    if (dataRef.size == 0) {
      throw Exception(
        'Aún no has consumido nada por lo que no hay información para mostrar',
      );
    }
    List<QueryDocumentSnapshot<Object?>> data = dataRef.docs;
    List<BarCharDataModel> resp = [];
    for (int i = 0; i < data[0]['data'].length; i++) {
      resp.add(
        BarCharDataModel(
          domain: '$i',
          measure: data[0]['data'][i].round(),
        ),
      );
    }
    return resp;
  }

  Future<Map<String, dynamic>> getUserFinantialWaste(String uid) async {
    final metric = FirebasePerformance.instance
        .newHttpMetric("https://www.google.com", HttpMethod.Get);
    await metric.start();
    QuerySnapshot<Object?> dataRef = await finantialWasteReference
        .where(
          'uid',
          isEqualTo: uid,
        )
        .get();
    await metric.stop();
    if (dataRef.size == 0) {
      throw Exception(
        'Aún no has consumido nada por lo que no hay información para mostrar',
      );
    }
    List<QueryDocumentSnapshot<Object?>> data = dataRef.docs;
    return data[0]['weeklyWaste'];
  }
}
