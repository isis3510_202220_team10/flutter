import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:main_app/helpers/hive_boxes_helper.dart';
import 'package:main_app/models/ingredient_model.dart';

class IngredientsService {
  //ATTRIBUTES
  static final IngredientsService _instance = IngredientsService._internal();

  ValueListenable<Box<IngredientModel>> getShoppingList() =>
      HiveBoxesHelper.getShoppingList().listenable();

  void editShoppingListIngredient(IngredientModel ingredient) =>
      HiveBoxesHelper.editShoppingListIngredient(ingredient);

  void deleteShoppingListIngredient(String reference) =>
      HiveBoxesHelper.deleteIngredientFromShoppingList(reference);

  factory IngredientsService() {
    return _instance;
  }

  IngredientsService._internal();
}
