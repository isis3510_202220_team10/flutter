import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:main_app/helpers/hive_boxes_helper.dart';
import 'package:main_app/models/recipe_model.dart';
import 'package:main_app/models/user_model.dart';
import 'package:main_app/ui/views/shopping_list/shopping_list_item.dart';
import 'package:main_app/ui/widgets/technological_expertise_dialog_view_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/ingredient_model.dart';

class UserService {
  static final UserService _instance = UserService._internal();
  final StreamController<List<IngredientModel>> shoppingListStreamController =
      StreamController<List<IngredientModel>>.broadcast();
  final StreamController<List<RecipeModel>> favoritesStreamController =
      StreamController<List<RecipeModel>>.broadcast();
  final StreamController<List<RecipeModel>> recomendationsStreamController =
      StreamController<List<RecipeModel>>.broadcast();
  late String userId;
  Stream<List<IngredientModel>> get shoppingList =>
      shoppingListStreamController.stream;
  Stream<List<RecipeModel>> get favorites => favoritesStreamController.stream;

  Stream<List<RecipeModel>> get recomendations =>
      recomendationsStreamController.stream;

  factory UserService() => _instance;

  UserService._internal();

  static final CollectionReference favoritesReference =
      FirebaseFirestore.instance.collection('favorites');
  static final CollectionReference usersReference =
      FirebaseFirestore.instance.collection("users");
  static final CollectionReference t3n4 =
      FirebaseFirestore.instance.collection("t3n4");

  static final CollectionReference recomendationsReference =
      FirebaseFirestore.instance.collection('Recomendations');

  Future<List<dynamic>> getUserFavorites() async {
    DocumentSnapshot docSnapshot = await favoritesReference.doc(userId).get();
    if (docSnapshot.exists) {
      Map<String, dynamic> data = docSnapshot.data() as Map<String, dynamic>;
      return data['favorites'];
    }

    return [] as List<String>;
  }

  Future<List<dynamic>> getUserRecomendations() async {
    DocumentSnapshot docSnapshot =
        await recomendationsReference.doc(userId).get();
    if (docSnapshot.exists) {
      Map<String, dynamic> data = docSnapshot.data() as Map<String, dynamic>;
      return data['recomendations'];
    }

    return [] as List<String>;
  }

  void updateSkill(int time) {
    UserModel user = getUser()!;
    DocumentReference doc = t3n4.doc(user.technicalSkill);
    doc.get().then((docSnapshot) {
      if (docSnapshot.exists) {
        Map<String, dynamic> info = docSnapshot.data() as Map<String, dynamic>;
        info['times'] = [...info['times'], time];
        doc.update(info);
      } else {
        doc.set({
          'times': [time]
        });
      }
    });
  }

  void emptyFavoritesList() {
    UserModel user = getUser()!;
    user.favorites = [];
    updateUser(user);
  }

  UserModel? getUser() => HiveBoxesHelper.getUser(userId);

  void addToShoppingList(List<IngredientModel> ingredients) {
    UserModel user = getUser()!;
    List<IngredientModel> shoppingList = user.shoppingList;
    IngredientModel act;
    int searchIndex;
    for (int i = 0; i < ingredients.length; i++) {
      act = ingredients[i];
      searchIndex = shoppingList
          .indexWhere((element) => element.reference == act.reference);
      if (searchIndex != -1) {
        if (shoppingList[searchIndex].quantity != null &&
            act.quantity != null) {
          shoppingList[searchIndex].quantity =
              shoppingList[searchIndex].quantity! + act.quantity!;
        }
      } else {
        act = IngredientModel.fromJson(ingredients[i].toJson());
        shoppingList.add(act);
      }
    }
    user.shoppingList = shoppingList;
    updateUser(user);
  }

  void deleteItemFromShoppingList(String reference) {
    UserModel user = getUser()!;
    int indexToDelete =
        user.shoppingList.indexWhere((item) => item.reference == reference);
    user.shoppingList.removeAt(indexToDelete);
    updateUser(user);
  }

  void editShoppingListIngredient(IngredientModel ingredient) {
    UserModel user = getUser()!;
    int indexToEdit = user.shoppingList
        .indexWhere((item) => item.reference == ingredient.reference);
    user.shoppingList[indexToEdit] = ingredient;
    updateUser(user);
  }

  void updateUser(UserModel user) {
    HiveBoxesHelper.putUser(user);
    shoppingListStreamController.sink.add(user.shoppingList);
    favoritesStreamController.sink.add(user.favorites);
    recomendationsStreamController.sink.add(user.recomendations);
  }

  void updateUserFirebase(UserModel user) {
    usersReference.doc(user.id).set(user.toJson());
  }

  void answeredTechnicalQuestion(String technicalSkill) {
    UserModel user = getUser()!;
    user.technicalSkill = technicalSkill;
    HiveBoxesHelper.putUser(user);
    updateUserFirebase(user);
  }

  Future<void> toggleFavorite(RecipeModel recipe) async {
    DocumentReference doc = favoritesReference.doc(userId);
    UserModel user = getUser()!;
    user.toggleFavorite(recipe);
    updateUser(user);
    recomendationsStreamController.sink.add([]);
    doc.get().then((DocumentSnapshot docSnapshot) {
      if (docSnapshot.exists) {
        if (recipe.favorite) {
          favoritesReference.doc(userId).update({
            "favorites": FieldValue.arrayUnion([recipe.id]),
          });
        } else {
          favoritesReference.doc(userId).update({
            "favorites": FieldValue.arrayRemove([recipe.id]),
          });
        }
      } else {
        if (recipe.favorite) {
          favoritesReference.doc(userId).set({
            "favorites": FieldValue.arrayUnion([recipe.id]),
          });
        } else {
          favoritesReference.doc(userId).set({
            "favorites": FieldValue.arrayRemove([recipe.id]),
          });
        }
      }
    });
  }

  Future<void> toggleRecomendation(RecipeModel recipe) async {
    DocumentReference doc = recomendationsReference.doc(userId);
    UserModel user = getUser()!;
    user.toggleRecomendation(recipe);
    updateUser(user);
    doc.get().then((DocumentSnapshot docSnapshot) {
      if (docSnapshot.exists) {
        if (recipe.recomended) {
          recomendationsReference.doc(userId).update({
            "recomendations": FieldValue.arrayUnion([recipe.id]),
          });
        } else {
          recomendationsReference.doc(userId).update({
            "recomendations": FieldValue.arrayRemove([recipe.id]),
          });
        }
      } else {
        if (recipe.recomended) {
          recomendationsReference.doc(userId).set({
            "recomendations": FieldValue.arrayUnion([recipe.id]),
          });
        } else {
          favoritesReference.doc(userId).set({
            "recomendations": FieldValue.arrayRemove([recipe.id]),
          });
        }
      }
    });
  }
}
