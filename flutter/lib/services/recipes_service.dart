import 'dart:ffi';

import 'package:async/async.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';
import 'package:main_app/helpers/hive_boxes_helper.dart';
import 'package:main_app/models/recipe_model.dart';
import 'package:main_app/services/user_service.dart';


//Service to access the recipes in the data base
class RecipesService {
  //ATTRIBUTES
  static final RecipesService _instance = RecipesService._internal();
  static final UserService userService = UserService();
  int limit = 6;
  List<RecipeModel> _recipes = [];
  List<RecipeModel> _recomendedRecipes = [];
  DocumentSnapshot? _lastDoc;
  bool _gettingMoreRecipes = false;
  bool _moreRecipesAvaliable = true;
  List<String> sortByOptions = ['name', 'rate', 'duration'];
  List<String> sortByOptionsSpanish = ['Nombre', 'Calificación', 'Duración'];
  int sortBy = 0;
  final CollectionReference recipesReference =
      FirebaseFirestore.instance.collection('recipes');

  factory RecipesService() {
    return _instance;
  }

  RecipesService._internal();

  //METHODS

  void sortQueryBy(int attr) => sortBy = attr;

  Future<List<RecipeModel>> getRecipes() async {
    _recipes = [];
    Query query = recipesReference.orderBy(sortByOptions[sortBy]).limit(limit);
    return _getUserFavorites().then(
      (value) => _getRecipes(query),
    );
  }

  Future<List<RecipeModel>> getRecomendedRecipes() async {
    _recipes = [];
    Query query = recipesReference;
    return _getRecomendedRecipes(query);
  }

  Future<List<RecipeModel>> getMoreRecipes() async {
    Query query = recipesReference
        .orderBy(sortByOptions[sortBy])
        .startAfterDocument(_lastDoc!)
        .limit(limit);
    return _getRecipes(query);
  }

  Future<List<RecipeModel>> _getRecipes(Query query) async {
    if (!_moreRecipesAvaliable || _gettingMoreRecipes) {
      return _recipes;
    }
    _gettingMoreRecipes = true;
    QuerySnapshot querySnapshot = await query.get();
    List<QueryDocumentSnapshot<Object?>> recipesJson = querySnapshot.docs;
    if (recipesJson.length < limit) {
      _moreRecipesAvaliable = false;
    }
    _lastDoc = recipesJson.last;
    _gettingMoreRecipes = false;
    return castRecipes(recipesJson);
  }

  Future<List<RecipeModel>> _getRecomendedRecipes(Query query) async {
    QuerySnapshot querySnapshot = await query.get();
    List<QueryDocumentSnapshot<Object?>> recipesJson = querySnapshot.docs;
    _gettingMoreRecipes = true;
    if (recipesJson.length < limit) {
      _moreRecipesAvaliable = false;
    }
    _lastDoc = recipesJson.last;
    _gettingMoreRecipes = false;
    return recomendationRecipes(recipesJson);
  }

    Future<List<RecipeModel>> castRecipes(
      List<QueryDocumentSnapshot<Object?>> recipesJson) async {
    Map<String, dynamic>? data;
    RecipeModel? recipe;
    for (int i = 0; i < recipesJson.length; i++) {
      data = recipesJson[i].data() as Map<String, dynamic>;
      data['id'] = recipesJson[i].id;
      recipe = RecipeModel.fromJson(data);
      _recipes.add(recipe);
    }
    return _recipes;
  }

  Future<List<RecipeModel>> recomendationRecipes(
      List<QueryDocumentSnapshot<Object?>> recipesJson) async {
    double duration = 0;
    double rate = 0;
    double calories = 0;
    int counter = 0;
    Map<String, dynamic>? data;
    RecipeModel recipe;
    for (int i = 0; i < recipesJson.length; i++) {
      data = recipesJson[i].data() as Map<String, dynamic>;
      data['id'] = recipesJson[i].id;
      recipe = RecipeModel.fromJson(data);
      if (await RecipesService.isFavorite(recipe.id)) {
        duration += recipe.duration;
        rate += recipe.rate;
        calories += recipe.calories;
        counter++;
      }
    }
    duration = duration / counter;
    rate = rate / counter;
    calories = calories / counter;
    List<RecipeModel> userFavorites = userService.getUser()!.favorites;
    for (int i = 0; i < recipesJson.length && _recomendedRecipes.length < 5; i++) {
      data = recipesJson[i].data() as Map<String, dynamic>;
      data['id'] = recipesJson[i].id;
      recipe = RecipeModel.fromJson(data);
      if (userFavorites.indexWhere((element) => element.id == recipe.id,)==-1 &&
      (recipe.duration >= duration - 50) &&
          (recipe.duration <= duration + 50) &&
          (recipe.rate >= rate - 1) &&
          (recipe.rate <= rate + 1) &&
          (recipe.calories >= calories - 1000) &&
          (recipe.calories <= calories + 1000)
          ) {
        _recomendedRecipes.add(recipe);
      }
    }
    return _recomendedRecipes;
  }

  Future<void> _getUserFavorites() async {
    Map<String, dynamic> data;
    RecipeModel act;
    userService.emptyFavoritesList();
    userService.getUserFavorites().then((favoriteIds) {
      for (int i = 0; i < favoriteIds.length; i++) {
        recipesReference.doc(favoriteIds[i]).get().then((docSanpshot) {
          if (docSanpshot.exists && docSanpshot.data() != null) {
            data = docSanpshot.data()! as Map<String, dynamic>;
            int indexAt =
                _recipes.indexWhere((element) => element.id == docSanpshot.id);
            if (indexAt == -1) {
              data['id'] = docSanpshot.id;
              act = RecipeModel.fromJson(data);
              act.toggleFavorite();
              _recipes.add(act);
            } else if (!_recipes[indexAt].favorite) {
              _recipes[indexAt].toggleFavorite();
            }
          }
        });
      }
    });
  }

  static Future<bool> isFavorite(String recipeId) async {
    List<dynamic> ids = await userService.getUserFavorites();
    return ids.contains(recipeId);
  }
}
