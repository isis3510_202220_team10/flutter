import 'package:flutter/material.dart';
import 'package:ui_shared/ui_shared.dart';

ThemeData primaryTheme() {
  return ThemeData(
    primaryColor: UIColors.middleRed,
    colorScheme: const ColorScheme(
      brightness: Brightness.light,
      primary: UIColors.middleRed,
      onPrimary: UIColors.blackCoffee,
      secondary: UIColors.middleRed,
      onSecondary: UIColors.blackCoffee,
      error: UIColors.error,
      onError: UIColors.blackCoffee,
      background: UIColors.eggshell,
      onBackground: UIColors.blackCoffee,
      surface: UIColors.middleRed,
      onSurface: UIColors.blackCoffee,
    ),
    scaffoldBackgroundColor: UIColors.eggshell,
    textTheme: const TextTheme(
      bodyText2: TextStyle(
        color: UIColors.blackCoffee,
      ),
    ),
  );
}
