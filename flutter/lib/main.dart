import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:main_app/Themes/primary_theme.dart';
import 'package:main_app/app_router.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:main_app/models/user_model.dart';
import 'package:main_app/ui/views/main_view/splash_screen.dart';
import 'package:main_app/ui/views/screens.dart';
import 'package:main_app/models/ingredient_model.dart';
import 'package:main_app/models/recipe_model.dart';
import 'package:provider/provider.dart';
import 'firebase_options.dart';
import './providers/auth.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await hiveSetup();
  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;
  runApp(const MyApp());
}

Future<void> hiveSetup() async {
  await Hive.initFlutter();
  Hive.registerAdapter(UserModelAdapter());
  Hive.registerAdapter(IngredientModelAdapter());
  Hive.registerAdapter(RecipeModelAdapter());
  await Hive.openBox<UserModel>(UserModel.hiveBoxName);
  await Hive.openBox<RecipeModel>(RecipeModel.hiveBoxName);
  await Hive.openBox<IngredientModel>(IngredientModel.shoppingListHiveBoxName);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
      ],
      child: Consumer<Auth>(
        builder: (context, auth, _) => MaterialApp(
          title: 'Flutter Demo',
          theme: primaryTheme(),
          home: auth.isAuth
              ? MainView()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (context, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
          routes: AppRouter.routes,
        ),
      ),
    );
  }
}
