import 'package:flutter/material.dart';
import 'package:main_app/ui/views/screens.dart';

class AppRouter {
  String initialRoute = AuthScreen.routeName;

  static Map<String, Widget Function(BuildContext)> routes = {
    AuthScreen.routeName: (BuildContext context) => AuthScreen(),
    AddRecipeScreen.routeName: (BuildContext context) =>
        const AddRecipeScreen(),
    ShoppingCartScreen.routeName: (BuildContext context) =>
        const ShoppingCartScreen(),
    RecipeList.routeName: (BuildContext context) => const RecipeList(),
    RecipeDetail.routeName: (BuildContext context) => const RecipeDetail(),
    MainView.routeName: (BuildContext context) => MainView(),
    EditRecipeScreen.routeName: (BuildContext context) =>
        const EditRecipeScreen(),
  };
}
